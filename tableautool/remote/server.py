import time

import tableauserverclient as TSC
from tableauserverclient import ServerResponseError

from tableautool.remote.datasource import DatasourceItem
from tableautool.remote.workbook import WorkbookItem

class TableauServerError(Exception):
    pass

class Server(TSC.server.server.Server):
    def __init__(self, server_address, use_server_version=False):
        super().__init__(server_address, use_server_version)
        # for k in vars(server):
        #     setattr(self, k, vars(server)[k])

    def get_all_projects(self):
        projects, pagination_item = self.projects.get()
        return projects

    def get_all_workbooks(self):
        workbooks, pagination_item = self.workbooks.get()
        return [WorkbookItem(self, workbook) for workbook in workbooks]

    def get_all_datasources(self):
        datasources, pagination_item = self.datasources.get()
        return [DatasourceItem(self, datasource) for datasource in datasources]

    def get_workbook(self, name=None, id=None, project_id=None):
        if project_id and name:
            return self.get_workbook_by_name_and_project_id(name, project_id)

        if name:
            return self.get_workbook_by_name(name)

        if id:
            return self.get_workbook_by_id(id)

        if not name and not id:
            raise ValueError('server.get_workbook() requires either workbook name or id')

    def get_workbook_by_name(self, name):
        matches = [w for w in self.get_all_workbooks() if w.name == name]
        if len(matches) > 1:
            raise UserWarning(
                f"More then one workbook found with name '{name}'. "
                f"Returning the first one.")

        if len(matches) == 0:
            raise KeyError(f'Workbook name={name} not found on server')
        return matches[0]

    def get_workbook_by_id(self, id):
        matches = [w for w in self.get_all_workbooks() if w.id == id]
        if len(matches) == 0:
            raise KeyError(f'Workbook {id} not found on server')
        return matches[0]

    def get_datasource(self, name=None, id=None, project_id=None):
        if project_id and name:
            return self.get_datasource_by_name_and_project_id(name, project_id)

        if id:
            return self.get_datasource_by_id(id)

        if name and not project_id:
            raise ValueError('Must provide both datasource name and project ID to ensure uniqueness')

        if not name and not id:
            raise ValueError('server.get_datasource() requires either workbook name or id')

    def get_datasource_by_name(self, name):
        datasources = [d for d in self.get_all_datasources() if d.name == name]
        if len(datasources) > 1:
            raise UserWarning(
                f"More then one datasource found with name '{name}'. "
                f"Returning the first one.")

        return datasources[0]

    def get_datasource_by_id(self, id):
        matching_datasources = [w for w in self.get_all_datasources() if w.id == id]

        if len(matching_datasources) == 0:
            raise KeyError(f'Datasource {id} not found on server')
        return matching_datasources[0]

    def get_workbook_by_name_and_project_id(self, name, project_id):
        matches = [wb for wb in self.get_all_workbooks()
                                if wb.name == name and wb.project_id == project_id]

        if len(matches) == 0:
            raise KeyError(f'Workbook with name={name} and project_id={project_id} '
                           f'not found on server')
        return matches[0]

    def get_datasource_by_name_and_project_id(self, name, project_id):
        matching_datasources = [ds for ds in self.get_all_datasources()
                                if ds.name == name and ds.project_id == project_id]

        if len(matching_datasources) == 0:
            raise KeyError(f'Datasource with name={name} and project_id={project_id} '
                           f'not found on server')
        return matching_datasources[0]

    def get_project_by_name(self, name):
        matching_projects = [p for p in self.get_all_projects() if p.name == name]
        if len(matching_projects) == 0:
            raise KeyError(f'Datasource with name={name} '
                           f'not found on server')
        return matching_projects[0]

    def delete_datasource(self, id:str):
        self.datasources.delete(id)

    def delete_workbook(self, id:str):
        self.workbooks.delete(id)

    def poll_job_id(self, job_id, wait_time=5):
        job = self.jobs.get_by_id(job_id)
        print(f'Job started: {job}')
        while job.completed_at is None:
            try:
                job = self.jobs.get_by_id(job_id)
            except:
                print('Job error')

            print(f'{job.type} Job ID {job.id} progress: {job.progress}')
            time.sleep(wait_time)

        if not job.finish_code == '0':
            raise TableauServerError(f'Job ID {job.id} did not complete successfully. \n Tableau Error: {job.notes}')

        print(f'Job {job.id} completed successfully.')


def publish_workbook(server, project_id, file_path, new_workbook_name, connection_credentials,
                     overwrite=TSC.Server.PublishMode.Overwrite, as_job=False, skip_connection_check=False):
    file_path = str(file_path)
    if '.twb' not in file_path:
        file_path = file_path + '.twbx'

    new_workbook = TSC.WorkbookItem(project_id, name=new_workbook_name)
    new_workbook = server.workbooks.publish(new_workbook, file_path, overwrite,
                                            # connections=workbook.connections,
                                            connection_credentials=connection_credentials,
                                            as_job=as_job,
                                            skip_connection_check=skip_connection_check
                                            )
    if as_job:
        server.poll_job_id(new_workbook.id, wait_time=1)
        new_workbook.id = server.get_workbook_by_name(new_workbook)

    try:
        wb = server.get_workbook(id=new_workbook.id)
    except KeyError:
        time.sleep(1) # wait for server to update
        wb = server.get_workbook(id=new_workbook.id)

    print("Workbook published. ID: {0}".format(new_workbook.id))
    return new_workbook.id


def publish_datasource(server: TSC.server.server.Server, project_id: str, file_path: str, new_datasource_name: str,
                       connection_credentials: TSC.ConnectionCredentials,
                       overwrite: TSC.Server.PublishMode = TSC.Server.PublishMode.Overwrite, as_job: bool = False) -> str:
    file_path = str(file_path)
    if '.tds' not in file_path:
        file_path = file_path + '.tds'

    new_datasource = TSC.DatasourceItem(project_id, name=new_datasource_name)
    new_datasource = server.datasources.publish(new_datasource, file_path, overwrite,
                                                connection_credentials=connection_credentials,
                                                as_job=as_job
                                                )
    if as_job:
        server.poll_job_id(new_datasource.id, wait_time=1)
        new_datasource.id = server.get_datasource_by_name(new_datasource)

    try:
        ds = server.get_datasource(id=new_datasource.id)
    except KeyError:
        time.sleep(1) # wait for server to update
        ds = server.get_datasource(id=new_datasource.id)

    print("Datasource published. ID: {0}".format(new_datasource.id))
    return new_datasource.id

