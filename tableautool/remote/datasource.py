import os
import zipfile

from tableauserverclient import models


class DatasourceItem(models.datasource_item.DatasourceItem):
    def __init__(self, server, datasource):
        super().__init__(datasource.project_id, datasource.name)

        server.datasources.populate_connections(datasource)

        for k in vars(datasource):
            setattr(self, k, vars(datasource)[k])

        self.server = server # add server for convenience

    def __repr__(self):
        return f"DatasourceItem(name='{self.name}', description='{self.description}', owner_id='{self.owner_id}', " \
               f"id='{self.id}', project_id='{self.project_id}', project_name='{self.project_name}'," \
               f"datasource_type='{self.datasource_type}', updated_at='{self.updated_at}'," \
               f"use_remote_query_agent='{self.use_remote_query_agent}', ask_data_enablement='{self.ask_data_enablement}'," \
               f"connections='{self.connections}', content_url='{self.content_url}'," \
               f"has_extracts='{self.has_extracts}')"

    def download(self, destination_folder='.', include_extract=True):
        return self.server.datasources.download(
            self.id,
            filepath=destination_folder,
            include_extract=include_extract
        )

    def download_with_unzip(self, destination_folder='.', include_extract=True):
        file_path = self.download(destination_folder, include_extract)

        # unzip .tdsx files only; .tds files are not archives
        if file_path[-1] == 'x':
            dest_dir = os.path.splitext(file_path)[0]
            with zipfile.ZipFile(file_path, 'r') as zip_ref:
                zip_ref.extractall(dest_dir)

        return file_path

    def refresh(self):
        """
        Function launches a refresh job via the REST API and polls the datasource metadata on the server until
        the Last Refreshed time is updated.
        :return:
        """

        job_id = self.server.datasources.refresh(self).id
        print(f'Datasource {self.id} refresh job sent to queue. Job ID: {job_id}')

        self.server.poll_job_id(job_id, wait_time=5)
        ds = self.server.get_datasource(id=self.id)
        print(f'Datasource {ds.id} refreshed at {ds.updated_at}')


    def update_connections(self):
        for connection in self.connections:
            self.server.workbooks.update_connection(self, connection)

    @property
    def is_live(self):
        return not(self.has_extracts)

    @property
    def is_extract(self):
        return self.has_extracts

    @property
    def connections(self):
        return [ConnectionItem(self.server, c) for c in super().connections]


class ConnectionItem(models.connection_item.ConnectionItem):
    def __init__(self, server, connection):
        super().__init__()

        for k in vars(connection):
            setattr(self, k, vars(connection)[k])

        self.server = server # add server for convenience

    def __repr__(self):
        return \
            f"ConnectionItem(id='{self.id}', datasource_name='{self.datasource_name}', datasource_id='{self.datasource_id}', " \
            f"connection_type='{self.connection_type}', username='{self.username}', password='{self.password}', " \
            f"embed_password='{self.embed_password}', server_address='{self.server_address}', server_port='{self.server_port}'," \
            f"connection_credentials='{self.connection_credentials}')"