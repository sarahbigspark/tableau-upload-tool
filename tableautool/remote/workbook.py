import os
import zipfile

from tableauserverclient import models

from tableautool.remote.datasource import DatasourceItem, ConnectionItem


class WorkbookItem(models.workbook_item.WorkbookItem):
    """
    Helper class to handle workbook manipulations and add human-readable repr string
    """
    def __init__(self, server, workbook):
        super().__init__(workbook.project_id, workbook.name, workbook.show_tabs)

        # Must populate connections before getting connections property
        server.workbooks.populate_connections(workbook)

        for k in vars(workbook):
            setattr(self, k, vars(workbook)[k])

        self.server = server # add server for convenience


    def __repr__(self):
        return \
            f"WorkbookItem(name='{self.name}', description='{self.description}', owner_id='{self.owner_id}', " \
            f"id='{self.id}', project_id='{self.project_id}', project_name='{self.project_name}'," \
            f"connections='{self.connections}', content_url='{self.content_url}'," \
            f"webpage_url='{self.webpage_url}', created_at='{self.created_at}', updated_at='{self.updated_at}'," \
            f"show_tabs='{self.show_tabs}', size='{self.size}',"


    def get_datasources(self):
        datasource_ids = [c.datasource_id for c in self.connections]
        datasources = [self.server.datasources.get_by_id(d) for d in datasource_ids]
        return [DatasourceItem(self.server, datasource) for datasource in datasources]

    def refresh(self):
        """
        Function launches a refresh job via the REST API and polls the workbook metadata on the server until
        the Last Refreshed time is updated.
        :return:
        """
        if all([d.is_live for d in self.get_datasources()]):
            raise ValueError('Cannot run refresh on workbook with all data sources set to Live. '
                             'A refresh action is for updating Extract data sources.')

        job_id = self.server.workbooks.refresh(self).id
        print(f'Workbook {self.id} refresh job sent to queue. Job ID: {job_id}')

        self.server.poll_job_id(job_id, wait_time=5)
        wb = self.server.get_workbook(id=self.id)
        print(f'Workbook {wb.id} refreshed at {wb.updated_at}')
        return wb.id

    @property
    def connections(self):
        return [ConnectionItem(self.server, c) for c in super().connections]

    def download(self, destination_folder='.', include_extract=True):
        return self.server.workbooks.download(
            self.id,
            filepath=destination_folder,
            include_extract=include_extract
        )

    def download_with_unzip(self, destination_folder='.', include_extract=True):
        file_path = self.download(destination_folder, include_extract)

        # unzip .twbx files only; .twb files are not archives
        if file_path[-1] == 'x':
            dest_dir = os.path.splitext(file_path)[0]
            with zipfile.ZipFile(file_path, 'r') as zip_ref:
                zip_ref.extractall(dest_dir)

        return file_path