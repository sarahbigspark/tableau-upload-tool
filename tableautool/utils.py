import contextlib
from copy import deepcopy
import shutil
import zipfile
import tempfile
from xml.etree import ElementTree as ET

class XMLComparison:
    def __init__(self, xml1:ET.ElementTree, xml2:ET.ElementTree, exclude_tags=[]):
        self.mismatch_tag = []
        self.mismatch_text = []
        self.mismatch_tail = []
        self.mismatch_attrib = []
        self.mismatch_length = []
        # sorting in the match check mutates xml: take deep copy to prevent mutating objects
        xml1_copy = deepcopy(xml1)
        xml2_copy = deepcopy(xml2)

        # remove tags from xml before compare
        for excluded_tag in exclude_tags:
            for element in xml1_copy.findall(excluded_tag):
                xml1_copy.remove(element)

            for element in xml2_copy.findall(excluded_tag):
                xml2_copy.remove(element)

        self.elements_equal(xml1_copy, xml2_copy)
        self.mismatches = {
            'tag' : self.mismatch_tag,
            # 'text' : self.mismatch_text,
            # 'tail' : self.mismatch_tail,
            'attrib' : self.mismatch_attrib,
            'length' : self.mismatch_length
        }
        self.objects_match = all([len(self.mismatches[k])==0 for k in self.mismatches])

    def elements_equal(self, e1, e2, current_paths=['xml1','xml2']):
        current_paths = [cp + '.' + e.tag for (cp, e) in zip(current_paths, [e1, e2])]
        if e1.tag != e2.tag: self.mismatch_tag.append(dict(zip(current_paths, [e1, e2]))) #return False
        # if e1.text != e2.text: self.mismatch_text.append(dict(zip(current_paths, [e1, e2]))) #return False
        # if e1.tail != e2.tail: self.mismatch_tail.append(dict(zip(current_paths, [e1, e2]))) #return False
        if e1.attrib != e2.attrib: self.mismatch_attrib.append(dict(zip(current_paths, [e1, e2]))) #return False
        if len(e1) != len(e2): self.mismatch_length.append(dict(zip(current_paths, [e1, e2]))) #return False
        # sort children by tag to prevent mismatches due to ordering
        sort_element_children_by_tag(e1)
        sort_element_children_by_tag(e2)
        return [self.elements_equal(c1, c2, current_paths) for c1, c2 in zip(e1, e2)]

    def __repr__(self):
        return 'XMLComparison mismatches found: {} '.format({k:len(v) for k,v in self.mismatches.items()})

def sort_element_children_by_tag(parent):
    parent[:] = sorted(parent, key=lambda child: child.tag)

@contextlib.contextmanager
def temporary_directory(*args, **kwargs):
    d = tempfile.mkdtemp(*args, **kwargs)
    try:
        yield d
    finally:
        shutil.rmtree(d)


def unzip(zip_file, dest_dir):
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(dest_dir)
    return dest_dir


def do_zip(folder, zip_file):
    shutil.make_archive(zip_file, 'zip', folder)

def list_files_in_archive(zip_file):
    z = zipfile.ZipFile(zip_file)
    return z.namelist()