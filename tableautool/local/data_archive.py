import os
from os.path import basename, join, splitext
import shutil
from os.path import join, dirname

import tableautool.utils
# from tableautool.local.workbook import LocalWorkbook

class DataExtractionError(Exception):
    pass


def unpack_data_from_twbx(workbook_file:str, dest_dir: str):
    # save and unpack entire workbook
    full_wb_extracted = join(dest_dir, 'workbook')
    tableautool.utils.unzip(workbook_file, full_wb_extracted)

    # get data folder only
    old_data_dir = join(full_wb_extracted, 'Data')
    new_data_dir = join(dest_dir, 'Data')
    if os.path.isdir(old_data_dir):
        shutil.move(old_data_dir, dest_dir)
    else:
        os.makedirs(new_data_dir)

    # delete workbook (.twbx) and extracted directory
    shutil.rmtree(full_wb_extracted)
    return new_data_dir


def create_data_archive_from_twbx(dest_file: str, workbook_twbx: str, include_files:list=None):
    if os.path.exists(dest_file):
        os.remove(dest_file)

    # create destination dir
    unpacked_data_dir = join(splitext(dest_file)[0], 'archive')
    if os.path.exists(unpacked_data_dir):
        shutil.rmtree(unpacked_data_dir)
    os.makedirs(unpacked_data_dir)

    # do unpack
    unpack_data_from_twbx(workbook_twbx, unpacked_data_dir)
    if include_files:
        for root, dirs, files in os.walk(unpacked_data_dir):
            for file in files:
                if file not in include_files:
                    os.remove(os.path.join(root, file))

    # zip 'archive' folder
    archive_path = os.path.splitext(dest_file)[0]
    tableautool.utils.do_zip(unpacked_data_dir, archive_path)
    return dest_file


def create_empty_data_archive(dest_file='./Data.zip'):
    """ Method creates an empty data directory archive file to be used in case no extract file was available"""
    if os.path.exists(dest_file):
        os.remove(dest_file)

    archive_dir = join(dirname(dest_file), 'archive')
    contents = join(archive_dir, 'Data', 'Extracts')
    if os.path.exists(contents):
        shutil.rmtree(contents)
    os.makedirs(contents)
    zipname = splitext(dest_file)[0]
    tableautool.utils.do_zip(archive_dir, zipname)
    return dest_file


def create_data_archive(dest_dir, workbook_path):
    data_archive_path = join(dest_dir, 'Data.zip')

    if os.path.exists(data_archive_path):
        os.remove(data_archive_path)

    if workbook_path is not None:
        # create data archive from workbook if availabe
        create_data_archive_from_twbx(data_archive_path, workbook_path)
        if not os.path.exists(data_archive_path):
            raise DataExtractionError("Failed to extract data archive from workbook")
    else:
        create_empty_data_archive(data_archive_path)

    return data_archive_path

def get_hyper_filename_from_ds_name(ds_name):
    """
    eg get filename 'federated.0q8nhvy1tcxvje14bzy5q0.hyper' from datasource name 'federated.0q8nhvy1tcxvje14bzy5q05djv6q'
    :param ds_name:
    :return:
    """
    return ds_name[:32].replace('.', '_') + '.hyper'