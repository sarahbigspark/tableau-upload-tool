import os
import copy
from os.path import join, dirname
import re
import zipfile
from xml.etree import ElementTree as ET, ElementTree

import tableaudocumentapi as tda
from tableaudocumentapi.xfile import build_archive_file

import tableautool.utils
from tableautool.local.connection import ConnectionParser, ObjectModelEncapsulateRelation
from tableautool.local.data_archive import create_data_archive_from_twbx, create_empty_data_archive, create_data_archive, get_hyper_filename_from_ds_name
from tableautool.remote.datasource import DatasourceItem


class DatasourceFactory():

    @staticmethod
    def get_datasource_from_file(datasource_file):
        raw_ds = Datasource.from_file(datasource_file)
        ds = DatasourceFactory.get_datasource(raw_ds.xml)
        ds.filename = datasource_file
        ds.data_files = tableautool.utils.list_files_in_archive(ds.filename)
        return ds

    @staticmethod
    def get_datasource_from_xml(datasource_xml):

        # Generate appropriate datasource type
        if datasource_xml.find('repository-location') is not None:
            return PublishedDatasource(datasource_xml)
        else:
            if datasource_xml.find('extract') is not None:
                return EmbeddedExtractDatasource(datasource_xml)
            else:
                return EmbeddedDatasource(datasource_xml)

    @staticmethod
    def get_datasource(xml_or_file):
        if type(xml_or_file) in [ET.Element, ET.ElementTree]:
            return DatasourceFactory.get_datasource_from_xml(xml_or_file)
        elif os.path.isfile(xml_or_file):
            return DatasourceFactory.get_datasource_from_file(xml_or_file)
        else:
            raise ValueError('Must provide either xml as Element or path to datasource file')


class Datasource(tda.Datasource):

    def __init__(self, dsxml, filename:str=None):
        """
        :param dsxml: xml from which to generate datasource object
        :param data_archive: file path to data archive zipped file. Needed for save().
        :param filename: optional filename if loading from file
        """
        self.filename = None
        self.data_files = None
        super().__init__(dsxml, filename=filename)
        self._connection_parser = ConnectionParser(
            self._datasourceXML, version=self._version)
        self._connections = self._connection_parser.get_connections()
        self.xml = self._datasourceXML
        self._connection_key = None

    def _remove_metadata(self):

        for conn in self.connections:
            conn._remove_metadata()


    def save_as(self, new_filename, override_xml:ET.Element=None, data_archive_file=None):
        """
        Save our file with the name provided.

        Args:
            new_filename:  New name for the workbook file. String.

        Returns:
            Nothing.

        """
        self.filename = new_filename
        file_ext = os.path.splitext(new_filename)[1]

        if override_xml:
            xmltree = ET.ElementTree(override_xml)
        else:
            xmltree = self._datasourceTree

        if file_ext == '.tdsx':
            # .tdsx file needs zip with extract data folder. Either use the one specified in
            # self.data_archive or create an empty one to use.
            if not data_archive_file:
                raise ValueError("""
                    If saving as .tdsx, embedded data must be provided via the data_archive_file param. \n
                    This should be a Data.zip archive file containing folder structure: Data/Extract/file_name.hyper
                    If you wish to use the embedded data from a workbook, please use the method .save_as_publishable_datasource()
                    and provide the path to the workbook .twbx file.
                """)
            self.save_into_archive(xmltree, data_archive_file, new_filename, self.caption + '.tds')
        elif file_ext == '.tds':
            xmltree.write(new_filename, encoding="utf-8", xml_declaration=True)
        else:
            raise ValueError('Datasource must be saved as either .tdsx or .tds file')


    def save_into_archive(self, xml_tree, data_archive_file, tdsx_filename, tds_filename):
        # Saving an archive means extracting the contents into a temp folder,
        # saving the changes over the twb/tds in that folder, and then
        # packaging it back up into a zip with a very specific format
        # e.g. no empty files for directories, which Windows and Mac do by default

        tds_filename = os.path.basename(tds_filename)

        # Extract to temp directory
        with tableautool.utils.temporary_directory() as temp_path:
            tableautool.utils.unzip(data_archive_file, temp_path)

            # Write the new version of the file to the temp directory
            xml_tree.write(os.path.join(
                temp_path, tds_filename), encoding="utf-8", xml_declaration=True)

            # Write the new archive with the contents of the temp folder
            with zipfile.ZipFile(tdsx_filename, "w", compression=zipfile.ZIP_DEFLATED) as new_archive:
                build_archive_file(temp_path, new_archive)

    def __eq__(self, other:tda.Datasource):

        # if one datasource has data present but the other does not, return not equal
        if (hasattr(self, 'data_files')) ^ (hasattr(other, 'data_files')):
            return False

        # if names of data files in data sources do not match, return not equal
        if self.data_files != other.data_files:
            return False

        # return boolean result of recursive check of equality of xml
        return tableautool.utils.XMLComparison(
            self.xml, other.xml, exclude_tags=['repository-location']
        ).objects_match

    @property
    def connection_key(self):
        return self.connections[0].connection_key

    @connection_key.setter
    def connection_key(self, value):
        self._connection_key = value



class PublishedDatasource(Datasource):

    def __init__(self, dsxml, filename:str=None):
        """
        :param dsxml: xml from which to generate datasource object
        :param data_archive: file path to data archive zipped file. Needed for save().
        :param filename: optional filename if loading from file
        """
        super().__init__(dsxml, filename=filename)
        self._repo_root = dsxml.find('repository-location')
        self._repo_derived_from = self._repo_root.get('derived-from')
        self._repo_id = self._repo_root.get('id')
        self._repo_path = self._repo_root.get('path')
        self._repo_revision = self._repo_root.get('revision')
        self._repo_site = self._repo_root.get('site')

    def point_to_new_published_datasource(self, new_datasource: DatasourceItem):
        xmltree = self._datasourceXML
        xmltree.set('caption', new_datasource.name)

        for conn in xmltree.findall('.//connection'):
            conn.set('dbname', new_datasource.content_url)
            conn.set('server-ds-friendly-name', new_datasource.name)

        for repo in xmltree.findall('.//repository-location'):
            # need to do a replace substring to get the right url:
            # use regex to find the old value and replace it.
            old_derived_from = repo.get('derived-from')
            old_ds_content_url = re.findall('datasources/(.+?)\?', old_derived_from)[0]
            new_derived_from = old_derived_from.replace(old_ds_content_url, new_datasource.content_url)

            repo.set('derived-from', new_derived_from)
            repo.set('id', new_datasource.content_url)

    @property
    def repo_derived_from(self):
        return self._repo_derived_from

    @repo_derived_from.setter
    def repo_derived_from(self, value):
        self._repo_derived_from = value
        self.repo_root.set('derived-from', value)

    @property
    def repo_id(self):
        return self._repo_id

    @repo_id.setter
    def repo_id(self, value):
        self._repo_id = value
        self.repo_root.set('id', value)

    @property
    def repo_path(self):
        return self._repo_path

    @repo_path.setter
    def repo_path(self, value):
        self._repo_path = value
        self.repo_root.set('path', value)

    @property
    def repo_revision(self):
        return self._repo_revision

    @repo_revision.setter
    def repo_revision(self, value):
        self._repo_revision = value
        self.repo_root.set('revision', value)

    @property
    def repo_site(self):
        return self._repo_site

    @repo_site.setter
    def repo_site(self, value):
        self._repo_site = value
        self.repo_root.set('site', value)



class EmbeddedDatasource(Datasource):

    def __init__(self, dsxml, filename:str=None):
        """
        :param dsxml: xml from which to generate datasource object
        :param data_archive: file path to data archive zipped file. Needed for save().
        :param filename: optional filename if loading from file
        """
        super().__init__(dsxml, filename=filename)

    def _clear_repository_location(self, xml):
        tag = xml.find('./repository-location')
        if tag is not None:
            xml.remove(tag)

    def _form_connection_element(self, server:str, content_url:str, datasource_name:str):
        """
        Example target XML:
        <connection channel='https' class='sqlproxy'
        dbname='table1sarahtableau12345db_16185139021110'
        directory='dataserver' port='443'
        saved-credentials-viewerid='79910'
        server='10ax.online.tableau.com'
        server-ds-friendly-name='table1 (sarahtableau12345db) (2)' username=''>
        :return:
        """

        connxml = ET.Element('connection')
        connxml.set('channel', 'https')
        connxml.set('class', 'sqlproxy')
        connxml.set('dbname', content_url)
        connxml.set('directory', 'dataserver')
        connxml.set('port', '443')
        connxml.set('saved-credentials-viewerid', '79910')
        connxml.set('server', server)
        connxml.set('server-ds-friendly-name', datasource_name)

        obj1 = ObjectModelEncapsulateRelation(label='_.fcp.ObjectModelEncapsulateLegacy.false...relation')\
            .set_to_sqlproxy()
        obj2 = ObjectModelEncapsulateRelation(label='_.fcp.ObjectModelEncapsulateLegacy.true...relation')\
            .set_to_sqlproxy()

        connxml.insert(1, obj1.xml)
        connxml.insert(2, obj2.xml)
        return connxml

    def _form_repository_location_element(self, site:str, content_url:str, revision='1.1'):
        """
        Method forms a repository location element required for re-pointing a workbook to a pubilished datasource.
        :param server_ds:
        :param site: site name, eg 'FSA'
        :return: xml.etree.ElementTree.Element
        """
        repoxml = ET.Element('repository-location')
        repoxml.set('derived-from', f'http://localhost:9100/t/{site}/datasources/{content_url}?rev={revision}')
        repoxml.set('id', content_url)
        repoxml.set('path', f'/t/{site}/datasources')
        repoxml.set('revision', revision)
        repoxml.set('site', site)
        return repoxml

    def _remove_extract(self, xml):
        extract = xml.find('extract')
        if xml is not None:
            xml.remove(extract)

    def _amend_object_relation(self, xml):
        """
        For node datasource.<_.fcp.ObjectModelEncapsulateLegacy.true...object-graph>.objects.object.properties context=""
        Target:

        <_.fcp.ObjectModelEncapsulateLegacy.true...object-graph>
            <objects>
              <object caption="table1" id="table1_943AE31B70D14F59A90B955439AB4AEC">
                <properties context="">
                  <relation name="sqlproxy" table="[sqlproxy]" type="table" />
                </properties>
              </object>
            </objects>
          </_.fcp.ObjectModelEncapsulateLegacy.true...object-graph>

        Example original:

        <_.fcp.ObjectModelEncapsulateLegacy.true...object-graph>
            <objects>
              <object caption="table1" id="table1_943AE31B70D14F59A90B955439AB4AEC">
                <properties context="">
                  <relation connection="azure_sqldb.01e7h3q0km7wu81g3yvfe0wgz6jg" name="table1" table="[dbo].[table1]" type="table" />
                </properties>
                <properties context="extract">
                  <relation name="Extract" table="[Extract].[Extract]" type="table" />
                </properties>
              </object>
            </objects>
          </_.fcp.ObjectModelEncapsulateLegacy.true...object-graph>

        :return:
        """
        root = xml.find('_.fcp.ObjectModelEncapsulateLegacy.true...object-graph')
        for obj in root.find('objects').findall('object'):
            relations = obj.findall(".//properties[@context='']")
            for rel in relations:
                ob_rel = ObjectModelEncapsulateRelation(rel.find('relation'))
                ob_rel.set_to_sqlproxy()

            extract_references = obj.findall(".//properties[@context='extract']")
            for extract_reference in extract_references:
                obj.remove(extract_reference)

    def _get_document_format_change_manifest(self):

        xml = ET.Element('document-format-change-manifest')
        xml.append(ET.Element('_.fcp.ObjectModelEncapsulateLegacy.true...ObjectModelEncapsulateLegacy'))
        xml.append(ET.Element('_.fcp.ObjectModelExtractV2.true...ObjectModelExtractV2'))
        xml.append(ET.Element('_.fcp.ObjectModelTableType.true...ObjectModelTableType'))
        xml.append(ET.Element('_.fcp.SchemaViewerObjectModel.true...SchemaViewerObjectModel'))
        # template_xml = """
        #   <document-format-change-manifest>
        #     <_.fcp.ObjectModelEncapsulateLegacy.true...ObjectModelEncapsulateLegacy />
        #     <_.fcp.ObjectModelExtractV2.true...ObjectModelExtractV2 />
        #     <_.fcp.ObjectModelTableType.true...ObjectModelTableType />
        #     <_.fcp.SchemaViewerObjectModel.true...SchemaViewerObjectModel />
        #   </document-format-change-manifest>
        # """
        return xml

    def _create_publishable_xml(self):
        """
        Generate xml to create publishable datasource: this xml should
        be provided to DatasourceFactory.get_datasource() to produce a
        valid PublishedDatasource object.
        Make updates to a deep copy of the xml because elementtree is mutable
        and we don't want this EmbeddedDatasource object to have the xml of
        a PublishedDatasource object
        :return:
        """
        xml = copy.deepcopy(self.xml)

        xml.set('formatted-name', self.name)
        xml.set('inline', 'true')
        xml.set('source-platform', 'linux')
        xml.set('xml:base', 'http://localhost:9100')
        xml.set('xmlns:user', 'http://www.tableausoftware.com/xml/user')
        del xml.attrib['caption']
        del xml.attrib['name']

        xml.insert(0, self._get_document_format_change_manifest())

        return xml

    def save_as_publishable_datasource(self, save_path, workbook_path):
        xml = self._create_publishable_xml()
        tmp_archive_file = join(dirname(workbook_path), 'tmp.zip')
        create_data_archive_from_twbx(tmp_archive_file, workbook_path, include_files=[get_hyper_filename_from_ds_name(self.name)])
        self.save_as(save_path, override_xml=xml, data_archive_file=tmp_archive_file)
        os.remove(tmp_archive_file)
        return save_path

    def create_xml_for_repointed_datasource(self, server:str, site:str, content_url:str, datasource_name:str, revision='1.1'):
        """
        Changes to be made:
        1) datasources
            a) caption (server ds name: OK) :   DONE
            b) inline ('true': OK) :            DONE
            c) name (sqlproxyname: NOT OKAY):   NOT DONE
        1) datasource.repository-location
            a) derived-from (new ds caption: OK):   DONE
            b) id (new ds caption: OK) :          DONE
            c) path (site: OK)                      DONE
            d) revision (hardcoded: to review)      DONE
            e) site (site: OK)                      DONEE
        2) datasource.connection
            a) channel ('https' : OK)               DONE
            b) class ('sqlproxy': OK)               DONE
            c) dbname (new ds caption: OK)          DONE
            d) directory ('dataserver': OK)         DONE
            e) port ('443': to review)              DONE
            f) saved-credentials-viewerid ('79910': to review)
            g) server (server_url: OK)              DONE
            h) server-ds-friendly-name (NOT OK)     approximated
            i) username ('': OK)                    DONE
        3) datasource.connection.fcp.ObjectModelEncapsulateLegacy tags
            a) name ('sqlproxy': OK)
            b) table ('[sqlproxy]': OK)
            c) type ('table': OK)
        4) datasource.connection.metadata
            a) remove all metadata
        5) datasource._.fcp.ObjectModelEncapsulateLegacy.objects.object.properties.relation
            a) name ('sqlproxy': OK)
            b) table ('[sqlproxy]': OK)
            c) type ('table': OK)
        6) worksheets.worksheet.table.view.datasources.datasource
            a) caption (server ds name: OK)
            b) name (sqlproxyname: NOT OKAY)
        7) worksheets.worksheet.table.view.datasources.datasource-dependencies
            a) datasource (sqlproxyname: NOT OKAY)
        8) worksheets.worksheet.table.rows
            a) value (sqlproxyname : NOT OKAY)
        9) worksheets.worksheet.table.cols
            a) value (sqlproxyname : NOT OKAY)
        10) workbook.windows.window.viewpoint.highlight.color-one-way.field
            a) value (sqlproxyname : NOT OKAY)

        :param server:
        :param site:
        :param content_url:
        :param datasource_name:
        :param revision:
        :return:
        """
        new_xml = copy.deepcopy(self._datasourceXML)
        # change datasource caption to new datasource name
        new_xml.set('caption', datasource_name)

        # construct and add a reference to the repository: this tells the workbook which ds to point to
        repoxml = self._form_repository_location_element(site, content_url, revision)
        self._clear_repository_location(new_xml)
        new_xml.insert(1, repoxml)

        # form connection details appropriate for published workbook
        connxml = self._form_connection_element(server, content_url, datasource_name)
        # remove existing connections
        for child in new_xml:
            if child.tag == 'connection':
                new_xml.remove(child)
        new_xml.insert(2, connxml)
        # note: currently this breaks the datasource.connections attribute
        # todo: fix

        # remove any referencees to extracts stored in the workbook
        self._remove_extract(new_xml)

        # set object relation tags to point to sqlproxy
        self._amend_object_relation(new_xml)
        return new_xml


class EmbeddedExtractDatasource(EmbeddedDatasource):

    def __init__(self, dsxml, filename:str=None):
        """
        :param dsxml: xml from which to generate datasource object
        :param data_archive: file path to data archive zipped file. Needed for save().
        :param filename: optional filename if loading from file
        """
        super().__init__(dsxml, filename=filename)
        # Embedded datasources set to Extract have an extract tag which gives info about the hyper file
        self._extractxml = dsxml.find('extract').find('connection')
        self._extract_type = self._extractxml.get('class')
        self._extract_path = self._extractxml.get('dbname')
        self._extract_last_updated = self._extractxml.get('update-time')


    @property
    def extract_type(self):
        return self._extract_type

    @extract_type.setter
    def extract_type(self, value):
        self._extract_type = value
        self._extractxml.set('class', value)

    @property
    def extract_path(self):
        return self._extract_path

    @extract_path.setter
    def extract_path(self, value):
        self._extract_path = value
        self._extractxml.set('dbname', value)

    @property
    def extract_last_updated(self):
        return self._extract_last_updated

    @extract_last_updated.setter
    def extract_last_updated(self, value):
        self._extract_last_updated = value
        self._extractxml.set('update-time', value)


