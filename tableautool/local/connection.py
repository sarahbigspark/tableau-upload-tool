from xml.etree import ElementTree as ET, ElementTree
from tableaudocumentapi import Connection

class ObjectModelEncapsulateRelation:
    def __init__(self, xml=None, label:str=None):
        if xml is None:
            xml = ET.Element(label)
            if label is None:
                raise ValueError('Either valid xml or string label required')

        self.xml = xml
        self._name = self.xml.get('name')
        self._table = self.xml.get('table')
        self._type = self.xml.get('type')
        self._connection = self.xml.get('connection')

    def set_to_sqlproxy(self):
        self.name = 'sqlproxy'
        self.table = '[sqlproxy]'
        self.type = 'table'
        if self.connection is not None:
            del self.xml.attrib['connection']
        return self

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self.xml.set('name', value)
        self._name = value

    @property
    def table(self):
        return self._table

    @table.setter
    def table(self, value):
        self.xml.set('table', value)
        self._table = value

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        self.xml.set('type', value)
        self._type = value

    @property
    def connection(self):
        return self._connection

    @connection.setter
    def connection(self, value):
        self.xml.set('connection', value)
        self._connection = value


class LocalConnection(Connection):
    def __init__(self, connxml):
        super().__init__(connxml)
        self.xml = connxml
        self.connection_key = self.dbclass + ':' + self.dbname

    def _remove_metadata(self):
        metadata = self.xml.find('metadata-records')
        if metadata:
            self.xml.remove(metadata)

class SqlproxyConnection(LocalConnection):
    def __init__(self, connxml):
        super().__init__(connxml)


class FederatedConnection(LocalConnection):
    def __init__(self, connxml):
        """Connection is usually instantiated by passing in connection elements
        in a Data Source. If creating a connection from scratch you can call
        `from_attributes` passing in the connection attributes.

        """
        named_connxml = connxml.find('.//named-connections/named-connection/*')
        super().__init__(named_connxml)
        self._local_data_directory = named_connxml.get('directory', None)
        self._local_data_filename = named_connxml.get('filename', None)
        self.object_models = list(map(ObjectModelEncapsulateRelation,
                                      connxml.findall("./_.fcp.ObjectModelEncapsulateLegacy.false...relation")))
        self.object_models += list(map(ObjectModelEncapsulateRelation,
                                       connxml.findall("./_.fcp.ObjectModelEncapsulateLegacy.true...relation")))

    def set_to_sqlproxy(self):
        for obj in self.object_models:
            obj.set_to_sqlproxy()

    @property
    def local_data_directory(self):
        """Directory of data file within archive."""
        return self._local_data_directory

    @local_data_directory.setter
    def local_data_directory(self, value):
        """
        Set the connection's local data directory property.

        Args:
            value:  New directory value. String.

        Returns:
            Nothing.

        """
        self._local_data_directory = value
        self._connectionXML.set('directory', value)

    @property
    def local_data_filename(self):
        """Directory of data file within archive."""
        return self._local_data_filename

    @local_data_filename.setter
    def local_data_filename(self, value):
        """
        Set the connection's local data filename property.

        Args:
            value:  New filename value. String.

        Returns:
            Nothing.

        """
        self._local_data_filename = value
        self._connectionXML.set('filename', value)

    def __repr__(self):
        return f"FederatedConnection(server={self.server}, username={self.username}, dbclass={self.dbclass}, " \
               f"dbname={self.dbname}, authentication={self.authentication})"


class ConnectionParser(object):
    """Parser for detecting and extracting connections from differing Tableau file formats."""

    def __init__(self, datasource_xml, version):
        self._dsxml = datasource_xml
        self._dsversion = version

    def _extract_federated_connections(self, xml=None):
        if not xml:
            xml = self._dsxml

        connections = []
        connections.extend(map(FederatedConnection, xml.findall("./connection[@class='federated']")))
        # connections = list(map(FederatedConnection, xml.findall('.//named-connections/named-connection/*')))
        # 'sqlproxy' connections (Tableau Server Connections) are not embedded into named-connection elements
        # extract them manually for now
        connections.extend(map(SqlproxyConnection, xml.findall("./connection[@class='sqlproxy']")))
        return connections

    def _extract_legacy_connection(self, xml=None):
        if not xml:
            xml = self._dsxml
        return list(map(FederatedConnection, xml.findall('connection')))

    def get_connections(self, xml=None):
        """Find and return all connections based on file format version."""

        if float(self._dsversion) < 10:
            connections = self._extract_legacy_connection(xml)
        else:
            connections = self._extract_federated_connections(xml)
        return connections