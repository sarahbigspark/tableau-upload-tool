import os
import shutil
from os.path import join, dirname
import xml.etree.ElementTree as ET

import tableaudocumentapi as tda
from tableaudocumentapi.xfile import xml_open

import tableautool.utils
from tableautool.utils import temporary_directory
from tableautool.remote.server import Server
from tableautool.local.datasource import DatasourceFactory, Datasource
from tableautool.local.datasource import EmbeddedDatasource


class LocalWorkbook(tda.Workbook):
    def __init__(self, filename):
        """Open the workbook at `filename`. This will handle packaged and unpacked
        workbook files automatically. This will also parse Data Sources and Worksheets
        for access.

        """

        self._filename = filename

        self._name = None

        self._workbookTree = xml_open(self._filename, 'workbook')

        self._workbookRoot = self._workbookTree.getroot()

        # prepare our datasource objects
        # self._data_archive_path = join(dirname(self.filename), 'Data.zip')
        #
        # self._create_data_archive(archive_path=self._data_archive_path)

        self._datasources = self._prepare_datasources(
            self._workbookRoot)  # self.workbookRoot.find('datasources')

        self._datasource_index = self._prepare_datasource_index(self._datasources)

        self._worksheets = self._prepare_worksheets(
            self._workbookRoot, self._datasource_index
        )

    def _prepare_datasources(self, xml_root):
        datasources = []

        # loop through our datasources and append
        datasource_elements = xml_root.find('datasources')
        if datasource_elements is None:
            return []

        for datasource in datasource_elements:
            ds = DatasourceFactory.get_datasource(datasource)
            datasources.append(ds)

        return datasources

    def replace_datasource(self, datasource_idx:int, new_datasource:Datasource):
        all_datasources_root = self._workbookRoot.find('datasources')
        datasources_roots = all_datasources_root.getchildren()
        datasource_root = datasources_roots[datasource_idx]
        all_datasources_root.remove(datasource_root)
        all_datasources_root.insert(datasource_idx, new_datasource.xml)

        self._datasources = self._prepare_datasources(
            self._workbookRoot)  # self.workbookRoot.find('datasources')

    def save_all_embedded_datasources_as_publishable(self, output_dir):
        for ds in self.datasources:
            if isinstance(ds, EmbeddedDatasource):
                ds_save_path = join(output_dir, ds.caption + '.tdsx')
                ds.save_as_publishable_datasource(ds_save_path, self.filename)

    def repoint_datasources_to_published_datasources(self, server:Server, server_address:str, site:str, project_id:str):
        for i, datasource in enumerate(self.datasources):
            if isinstance(datasource, EmbeddedDatasource):
                # Look up relevant details from the server
                ds_name = datasource.caption
                server_ds = server.get_datasource(name=ds_name, project_id=project_id)

                # Apply details to datasource
                published_datasource_xml = datasource.create_xml_for_repointed_datasource(
                    server=server_address, site=site, content_url=server_ds.content_url,
                    datasource_name=server_ds.name
                )

                self.datasources[i] = DatasourceFactory.get_datasource(published_datasource_xml)


    def save_as(self, new_filename, replace_extracts_with:list=None):
        if replace_extracts_with is not None:
            self.save_as_with_replaced_extracts(new_filename, replace_extracts_with)
        else:
            super().save_as(new_filename)

    def save_as_with_unzip(self, new_filename):
        self.save_as(new_filename)
        dest_dir = os.path.splitext(new_filename)[0]
        if os.path.isdir(dest_dir):
            shutil.rmtree(dest_dir)
        return tableautool.utils.unzip(new_filename, dest_dir)

    def save_as_with_replaced_extracts(self, new_filename, replace_extracts_with:list):
        raise NotImplementedError

    @property
    def name(self):
        repo = self._workbookRoot.find('repository-location')
        if repo is not None:
            return repo.get('id')

    @name.setter
    def name(self, value):
        repo = self._workbookRoot.find('repository-location')
        if repo is not None:
            self._name = value
            repo.set('id', value)


