####
# This script contains functions that demonstrate how to publish
# a workbook using the Tableau Server REST API. It will publish a
# specified workbook to the 'default' project of a given server.
#
# Note: The REST API publish process cannot automatically include
# extracts or other resources that the workbook uses. Therefore,
# a .twb file with data from a local computer cannot be published.
# For simplicity, this sample will only accept a .twbx file to publish.
#
# For more information, refer to the documentations on 'Publish Workbook'
# (https://onlinehelp.tableau.com/current/api/rest_api/en-us/help.htm)
#
# To run the script, you must have installed Python 2.7.9 or later,
# plus the 'requests' library:
#   http://docs.python-requests.org/en/latest/
#
# The script takes in the server address and username as arguments,
# where the server address has no trailing slash (e.g. http://localhost).
# Run the script in terminal by entering:
#   python publish_sample.py <server_address> <username>
#
# When running the script, it will prompt for the following:
# 'Workbook file to publish': Enter file path to the desired workbook file
#                             to publish (.twbx file).
# 'Password':                 Enter password for the user to log in as.
####
import requests # Contains methods used to make HTTP requests
import xml.etree.ElementTree as ET # Contains methods used to build and parse XML
import sys
import os
import math
import getpass
import json
import argparse

# The following packages are used to build a multi-part/mixed request.
# They are contained in the 'requests' library
from requests.packages.urllib3.fields import RequestField
from requests.packages.urllib3.filepost import encode_multipart_formdata

VERSION = '3.8'

# The namespace for the REST API is 'http://tableausoftware.com/api' for Tableau Server 9.0
# or 'http://tableau.com/api' for Tableau Server 9.1 or later
xmlns = {'t': 'http://tableau.com/api'}

# The maximum size of a file that can be published in a single request is 64MB
# FILESIZE_LIMIT = 1024 * 1024 * 64   # 64MB
FILESIZE_LIMIT = 1024

# For when a workbook is over 64MB, break it into 5MB(standard chunk size) chunks
CHUNK_SIZE = 1024 * 1024 * 5    # 5MB



class RESTInterfaceMixin:
    def _check_status(self, server_response, success_code):
        """
        Checks the server response for possible errors.

        'server_response'       the response received from the server
        'success_code'          the expected success code for the response
        Throws an ApiCallError exception if the API call fails.
        """
        if server_response.status_code != success_code:
            parsed_response = ET.fromstring(server_response.text)

            # Obtain the 3 xml tags from the response: error, summary, and detail tags
            error_element = parsed_response.find('t:error', namespaces=xmlns)
            summary_element = parsed_response.find('.//t:summary', namespaces=xmlns)
            detail_element = parsed_response.find('.//t:detail', namespaces=xmlns)

            # Retrieve the error code, summary, and detail if the response contains them
            code = error_element.get('code', 'unknown') if error_element is not None else 'unknown code'
            summary = summary_element.text if summary_element is not None else 'unknown summary'
            detail = detail_element.text if detail_element is not None else 'unknown detail'
            error_message = '{0}: {1} - {2}'.format(code, summary, detail)
            raise ApiCallError(error_message)
        return

    def _make_multipart(self, parts):
        """
        Creates one "chunk" for a multi-part upload

        'parts' is a dictionary that provides key-value pairs of the format name: (filename, body, content_type).

        Returns the post body and the content type string.

        For more information, see this post:
            http://stackoverflow.com/questions/26299889/how-to-post-multipart-list-of-json-xml-files-using-python-requests
        """
        mime_multipart_parts = []
        for name, (filename, blob, content_type) in parts.items():
            multipart_part = RequestField(name=name, data=blob, filename=filename)
            multipart_part.make_multipart(content_type=content_type)
            mime_multipart_parts.append(multipart_part)

        post_body, content_type = encode_multipart_formdata(mime_multipart_parts)
        content_type = ''.join(('multipart/mixed',) + content_type.partition(';')[1:])
        return post_body, content_type

    def _encode_for_display(self, text):
        """
        Encodes strings so they can display as ASCII in a Windows terminal window.
        This function also encodes strings for processing by xml.etree.ElementTree functions.

        Returns an ASCII-encoded version of the text.
        Unicode characters are converted to ASCII placeholders (for example, "?").
        """
        return text.encode('ascii', errors="backslashreplace").decode('utf-8')


class WorkbookUploader(RESTInterfaceMixin):
    def __init__(self, session, project_id, workbook_path):
        self.session = session
        # self.server = server
        # self.site_id = site_id
        self.project_id = project_id
        # self.auth_token = auth_token
        self.workbook_path = workbook_path
        self.workbook_file = os.path.basename(workbook_path)
        self.workbook_filename, self.file_extension = os.path.splitext(self.workbook_file)

    def do_upload(self):
        raise NotImplementedError

    def _build_general_request(self):
        xml_request = ET.Element('tsRequest')
        workbook_element = ET.SubElement(xml_request, 'workbook', name=self.workbook_file)
        ET.SubElement(workbook_element, 'project', id=self.project_id)
        return ET.tostring(xml_request)

    @property
    def workbook_path(self):
        return self._workbook_path

    @workbook_path.setter
    def workbook_path(self, value):
        if not os.path.exists(value):
            raise IOError(f'Workbook path {value} does not exist')
        self._workbook_path = value

    @property
    def file_extension(self):
        return self._file_extension

    @file_extension.setter
    def file_extension(self, ext):
        accepted_extensions = ['twbx']
        ext = ext[ext.find('.')+1:] # remove leading '.' in extension
        if ext not in accepted_extensions:
            raise ValueError(f'Workbook extension {ext} not accepted. '
                             f'Accepted file extensions: {accepted_extensions}')
        self._file_extension = ext


class MultiChunkWorkbookUploader(WorkbookUploader):
    def __init__(self, session, project_id, workbook_path, chunk_size=None):
        super().__init__(session, project_id, workbook_path)
        self.chunk_size = chunk_size or CHUNK_SIZE

    def _start_multichunk_upload(self):
        """
        Creates a POST request that initiates a file upload session.

        'server'        specified server address
        'auth_token'    authentication token that grants user access to API calls
        'site_id'       ID of the site that the user is signed into
        Returns a session ID that is used by subsequent functions to identify the upload session.
        """
        url = self.session.server + "/api/{0}/sites/{1}/fileUploads".format(VERSION, self.session.site_id)

        headers = {'x-tableau-auth': self.session.auth_token}

        # server_response = requests.post(url, headers=headers)
        server_response = self.session.post(url, headers=headers)
        return server_response.find('t:fileUpload', namespaces=xmlns).get('uploadSessionId')


    def _upload_chunk(self, upload_id, data_chunk):
        # URL for PUT request to append chunks for publishing
        put_url = self.session.server + "/api/{0}/sites/{1}/fileUploads/{2}".format(VERSION, self.session.site_id, upload_id)

        payload, content_type = self._make_multipart({'request_payload': ('', '', 'text/xml'),
                                                 'tableau_file': ('file', data_chunk, 'application/octet-stream')})
        print("\tPublishing a chunk...")
        server_response = requests.put(put_url, data=payload,
                                       headers={'x-tableau-auth': self.auth_token, "content-type": content_type})
        return self._check_status(server_response, 200)

    def _complete_multichunk_upload(self, project_id, upload_id):
        xml_request = self._build_general_request()

        payload, content_type = self._make_multipart(
            {'request_payload': ('', xml_request, 'text/xml')}
        )

        publish_url = self.server + "/api/{0}/sites/{1}/workbooks".format(VERSION, self.site_id)
        publish_url += "?uploadSessionId={0}".format(upload_id)
        publish_url += "&workbookType={0}&overwrite=true".format(self.file_extension)

        # Make the request to publish and check status code
        print("\tUploading...")
        server_response = requests.post(publish_url, data=payload,
                                        headers={'x-tableau-auth': self.auth_token, 'content-type': content_type})
        return self._check_status(server_response, 201)

    def _get_data_bytes_chunks(self):
        with open(self.workbook_path, 'rb') as f:
            while True:
                data = f.read(self.chunk_size)
                if not data:
                    break
                yield data

    def do_upload(self):
        upload_id = self._start_multichunk_upload()

        for data_chunk in self._get_data_bytes_chunks():
            self._upload_chunk(upload_id, data_chunk)

        self._complete_multichunk_upload(self.project_id, upload_id)


class SingleChunkWorkbookUploader(WorkbookUploader):
    def __init__(self, server, site_id, project_id, auth_token, workbook_path):
        super().__init__(server, site_id, project_id, auth_token, workbook_path)

    def _get_data_bytes(self):
        with open(self.workbook_path, 'rb') as f:
            return f.read()

    def do_upload(self):
        xml_request = self._build_general_request()

        # Finish building request for all-in-one method
        parts = {'request_payload': ('', xml_request, 'text/xml'),
                 'tableau_workbook': (self.workbook_file, self._get_data_bytes(), 'application/octet-stream')}
        payload, content_type = self._make_multipart(parts)

        publish_url = self.server + "/api/{0}/sites/{1}/workbooks".format(VERSION, self.site_id)
        publish_url += "?workbookType={0}&overwrite=true".format(self.file_extension)

        # Make the request to publish and check status code
        print("\tUploading...")
        server_response = requests.post(publish_url, data=payload,
                                        headers={'x-tableau-auth': self.auth_token, 'content-type': content_type})

        self._check_status(server_response, 201)


class TableauSession(RESTInterfaceMixin):
    def __init__(self, server, site, personal_access_token_filepath):
        self.server = server
        self.site = site
        self.access_token_file = personal_access_token_filepath
        self.auth_token = None

    def _get_login_credentials(self):
        with open(self.access_token_file) as fi:
            data = json.loads(fi.read())

        return data['token_name'], data['token_secret']

    def sign_in(self):
        """
        Signs in to the server specified with the given credentials

        'server'   specified server address
        'username' is the name (not ID) of the user to sign in as.
                   Note that most of the functions in this example require that the user
                   have server administrator permissions.
        'password' is the password for the user.
        'site'     is the ID (as a string) of the site on the server to sign in to. The
                   default is "", which signs in to the default site.
        Returns the authentication token and the site ID.
        """
        token_name, token_secret = self._get_login_credentials()

        url = self.server + "/api/{0}/auth/signin".format(VERSION)

        # Builds the request
        xml_request = ET.Element('tsRequest')
        credentials_element = ET.SubElement(xml_request, 'credentials', personalAccessTokenName=token_name,
                                            personalAccessTokenSecret=token_secret)
        ET.SubElement(credentials_element, 'site', contentUrl=self.site)
        xml_request = ET.tostring(xml_request)

        # Make the request to server
        server_response = requests.post(url, data=xml_request)
        self._check_status(server_response, 200)

        # ASCII encode server response to enable displaying to console
        server_response = self._encode_for_display(server_response.text)

        # Reads and parses the response
        parsed_response = ET.fromstring(server_response)

        # Gets the auth token and site ID
        token = parsed_response.find('t:credentials', namespaces=xmlns).get('token')
        site_id = parsed_response.find('.//t:site', namespaces=xmlns).get('id')
        return token, site_id

    def sign_out(self):
        """
        Destroys the active session and invalidates authentication token.

        'server'        specified server address
        'auth_token'    authentication token that grants user access to API calls
        """
        url = self.server + "/api/{0}/auth/signout".format(VERSION)
        server_response = requests.post(url, headers={'x-tableau-auth': self.auth_token})
        self._check_status(server_response, 204)
        return

    def get_project_ids(self):
        """
        Returns a dict of projects and project IDs from the Tableau server.

        'server'        specified server address
        'auth_token'    authentication token that grants user access to API calls
        'site_id'       ID of the site that the user is signed into
        """
        page_num, page_size = 1, 100  # Default paginating values

        # Builds the request
        url = self.server + "/api/{0}/sites/{1}/projects".format(VERSION, self.site_id)
        paged_url = url + "?pageSize={0}&pageNumber={1}".format(page_size, page_num)
        server_response = requests.get(paged_url, headers={'x-tableau-auth': self.auth_token})
        self._check_status(server_response, 200)
        xml_response = ET.fromstring(self._encode_for_display(server_response.text))

        # Used to determine if more requests are required to find all projects on server
        total_projects = int(xml_response.find('t:pagination', namespaces=xmlns).get('totalAvailable'))
        max_page = int(math.ceil(total_projects / page_size))

        projects = xml_response.findall('.//t:project', namespaces=xmlns)

        # Continue querying if more projects exist on the server
        for page in range(2, max_page + 1):
            paged_url = url + "?pageSize={0}&pageNumber={1}".format(page_size, page)
            server_response = requests.get(paged_url, headers={'x-tableau-auth': self.auth_token})
            self._check_status(server_response, 200)
            xml_response = ET.fromstring(self._encode_for_display(server_response.text))
            projects.extend(xml_response.findall('.//t:project', namespaces=xmlns))

        # Look through all projects to find the 'default' one
        return {project.get('name') : project.get('id') for project in projects}

    def get_project_id(self, project_name):
        return self.get_project_ids()[project_name]

    def get_default_project_id(self):
        return self.get_project_id('default')

    def process_response(self, response):
        self._check_status(response, 201)
        xml_response = ET.fromstring(self._encode_for_display(response.text))
        return xml_response

    def put(self, url, payload=None, headers:dict=None):
        if payload:
            payload, content_type = self._make_multipart(payload)
        response = requests.put(url, data=payload, headers=headers)
        return self.process_response(response)


    def post(self, url, payload=None, headers:dict=None):
        if payload:
            payload, content_type = self._make_multipart(payload)
        response = requests.post(url, data=payload, headers=headers)
        return self.process_response(response)

    def get(self, url, payload=None, headers:dict=None):
        if payload:
            payload, content_type = self._make_multipart(payload)
            headers = headers or {}

        if headers:
            headers += {"content-type": content_type}

        response = requests.get(url, data=payload, headers=headers)
        return self.process_response(response)

    def my_decorator(self, func):
        def wrapper(url, payload=None, headers:dict=None):
            print("Something is happening before the function is called.")
            if payload:
                payload, content_type = self._make_multipart(payload)
                headers = headers or {}

            if headers:
                headers += {"content-type": content_type}

            response = func(url, payload=payload, headers=headers)
            print("Something is happening after the function is called.")
            return self.process_response(response)

        return wrapper

    def __enter__(self):
        self.auth_token, self.site_id = self.sign_in()
        return self

    def __exit__(self, type, value, traceback):
        self.sign_out()
        print(f'logged out of auth id {self.auth_token}')


class ApiCallError(Exception):
    pass


class UserDefinedFieldError(Exception):
    pass




def format_filesize(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def main():
    server = args.server
    site = args.site
    project = args.project
    workbook_file_path = args.workbook
    access_token_file = args.tableau_credentials_file

    with TableauSession(server, site, access_token_file) as session:

        # Get workbook size to check if chunking is necessary
        workbook_size = os.path.getsize(workbook_file_path)
        chunked = workbook_size >= FILESIZE_LIMIT
        print('Workbook size: {}'.format(format_filesize(workbook_size)))

        project_id = session.get_project_id(project)

        if chunked:
            MultiChunkWorkbookUploader(
                session = session,
                project_id = project_id,
                workbook_path = workbook_file_path,
                chunk_size = CHUNK_SIZE
            ).do_upload()
        else:
            SingleChunkWorkbookUploader(
                server = server,
                site_id = session.site_id,
                project_id = project_id,
                auth_token = session.auth_token,
                workbook_path = workbook_file_path
            ).do_upload()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Upload a workbook to Tableau Server.')

    parser.add_argument('--server', type=str, required=True, help='Tableau server to send the requests to, eg "https://10ax.online.tableau.com"')
    parser.add_argument('--site', type=str, required=True, help='Site on the Tableau server, eg "project"')
    parser.add_argument('--project', type=str, required=True, help='Site on the Tableau server, eg "default"')
    parser.add_argument('--workbook', type=str, required=True, help='Workbook file path, eg "./workbooks/workbook1.twbx"')
    parser.add_argument('--tableau_credentials_file', type=str, required=True, help="""
        Tableau access credentials must be provided as a json input file containing a Personal Access token in the 
        following format. This access token can be created in the Account Settings page of a Tableau Server session
        accessed from a web browser. Format: \n
            {
              "token_name" : "my_token_1",
              "token_secret" : "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            }
        """)
    args = parser.parse_args()

    main()


