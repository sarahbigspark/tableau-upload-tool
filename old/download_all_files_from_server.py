import os
import glob
import time
import shutil
import argparse
from datetime import timezone
import datetime
import zipfile

# official APIs
import tableauserverclient as TSC
import tableaudocumentapi as tda

# unofficial APIs
from tableau_tools.tableau_documents import TableauFileManager

from publish_workbook_tsc import ConnectionCredentials
from tableautool.remote.datasource import DatasourceItem, ConnectionItem
from tableautool.remote.workbook import WorkbookItem
from tableautool.remote.server import Server, publish_datasource
import config as cfg

def main():
    username = cfg.tableau_username
    password = cfg.tableau_password
    site_name = cfg.tableau_site_name
    server = cfg.tableau_server

    # tableau_auth = TableauAuth(token_name, token_secret, site_id=site_name)
    tableau_auth = TSC.TableauAuth(username, password, site_id=site_name)
    server = Server(server)
    server.version = '3.8'

    with server.auth.sign_in(tableau_auth):

        # download all datasources
        datasources = server.get_all_datasources()
        for ds in datasources:
            label = 'live' if ds.is_live else 'extract'
            dest = f'./data/datasources/{label}/{ds.id}'
            if not os.path.isdir(dest):
                os.makedirs(dest)
            try:
                ds.download_with_unzip(dest)
            except Exception as ex:
                print(ex)

        # download all workbooks
        workbooks = server.get_all_workbooks()
        for wb in workbooks:
            dest = f'./data/workbooks/{wb.id}'
            if not os.path.isdir(dest):
                os.makedirs(dest)
            try:
                wb.download_with_unzip(dest)
            except Exception as ex:
                print(ex)

    # extract all datasources from downloaded workbooks and save extracted xml
    for workbook_fn in glob.glob('./data/workbooks/*/*.twbx'):
        base_dir = workbook_fn.replace('workbooks', 'datasources_extracted').replace('.twbx', '')

        wb_unofficial = TableauFileManager.open(
            filename=workbook_fn
        )

        wb_official = tda.Workbook(
            filename=workbook_fn
        )

        for i, ds_unofficial in enumerate(wb_unofficial.datasources):
            ds_official = wb_official.datasources[i]
            is_published = 'published' if ds_unofficial.is_published else 'not_published'

            new_dir = os.path.join(base_dir, is_published)
            extracted_ds_fp = os.path.join(new_dir, ds_official.name + '.xml')
            if not os.path.isdir(new_dir):
                os.makedirs(new_dir)

            ds_str = ds_unofficial.get_xml_string()
            with open(extracted_ds_fp, 'w+') as fo:
                fo.write(ds_str.decode('utf-8'))

        print('done!')




if __name__ == '__main__':
    main()