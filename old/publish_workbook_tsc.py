import os
import time
import shutil
import argparse
import zipfile

import tableauserverclient as TSC
from tableauserverclient import ConnectionCredentials

from tableautool.remote.server import Server, publish_workbook, publish_datasource
import config as cfg

def create_tdsx_from_dir(path_to_zip, output_file):

    def zipdir(path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(path, '../..')))

    output_fn = os.path.splitext(output_file)[0]
    fn_zip = output_fn + '.zip'
    fn_tdsx = output_fn + '.tdsx'

    zipf = zipfile.ZipFile(fn_zip, 'w', zipfile.ZIP_DEFLATED)
    zipdir(path_to_zip, zipf)
    zipf.close()

    shutil.move(fn_zip, fn_tdsx)
    return fn_tdsx


def main():

    tableau_username = cfg.tableau_username
    tableau_password = cfg.tableau_password
    tableau_site_name = cfg.tableau_site_name
    tableas_server = cfg.tableau_server

    workbook_name = 'azuresql_multisource'

    # tableau_auth = TableauAuth(token_name, token_secret, site_id=site_name)
    tableau_auth = TSC.TableauAuth(tableau_username, tableau_password, site_id=tableau_site_name)
    server = Server(tableas_server)
    server.version = '3.8'

    overwrite_true = TSC.Server.PublishMode.Overwrite

    with server.auth.sign_in(tableau_auth):

        data_proj_id = server.get_project_by_name('Data').id

        working_file = 'data/datasources/extract/e10008ec-1c9c-4440-8b68-72289f69d6a4/table1 sarahtableau12345db.tdsx'

        # archive_dir = './data/datasources/extract/manualupdate/table1 sarahtableau12345db_manualupdate'
        archive_dir = './data/datasources/extract/e10008ec-1c9c-4440-8b68-72289f69d6a4/table1 sarahtableau12345db'
        archive_file = './data/datasources/extract/manualupdate/manualupdate2.tdsx'
        archive_file = create_tdsx_from_dir(archive_dir, archive_file)

        connection_credentials = ConnectionCredentials(
            name='sarah.scott@bigspark.dev',
            password='1A8VNmFJRAKe!',
            embed=True,
            oauth=False
        )

        tds_file = 'data/datasources/extract/e10008ec-1c9c-4440-8b68-72289f69d6a4/table1 sarahtableau12345db/table1 (sarahtableau12345db).tds'

        publish_datasource(server, data_proj_id, tds_file, 'manual update',
                           connection_credentials=connection_credentials)

        # download all datasources
        datasources = server.get_all_datasources()
        for ds in datasources:
            label = 'live' if ds.is_live else 'extract'
            dest = f'./data/datasources/{label}/{ds.id}'
            if not os.path.isdir(dest):
                os.makedirs(dest)
            try:
                file_path = ds.download_with_unzip(dest)
                publish_datasource(server, data_proj_id, file_path, ds.name,
                                   connection_credentials=connection_credentials)
            except Exception as ex:
                print(ex)

        # download all workbooks
        workbooks = server.get_all_workbooks()
        for wb in workbooks:
            dest = f'./data/workbooks/{wb.id}'
            if not os.path.isdir(dest):
                os.makedirs(dest)
            try:
                wb.download_with_unzip(dest)
            except Exception as ex:
                print(ex)



        datasource = datasources[0]
        # datasource.download()

        workbook = server.get_workbook(name=workbook_name)
        workbook_datasources = workbook.get_datasources()
        # workbook.refresh()

        db_server = cfg.db_server
        database = cfg.db_database
        username = cfg.db_username
        password = cfg.db_password

        # get workbook and connections

        workbook_name_old = workbook_name + '_old'
        workbook_name_new = workbook_name + '_new'
        local_dir = '../data_generation_scripts/data'
        old_path = os.path.join(local_dir, workbook_name_old)
        new_path = os.path.join(local_dir, workbook_name_new)
        creds = ConnectionCredentials("sqladmin", "WhoCouldHavePredictedThis???", True)
        project_id = server.get_all_projects()['default']


        # take backup of the file before refresh

        server.workbooks.download(workbook.id, filepath=old_path, include_extract=True)
        publish_workbook(server, project_id, old_path, workbook_name_old, creds,
                         as_job=False, skip_connection_check=True)


        # get workbook again for providing credentials
        for connection in workbook.connections:
            connection.username = username
            connection.password = password
            server.workbooks.update_connection(workbook, connection)

        # refresh workbook
        server.workbooks.refresh(workbook)

        # Final backup for easy comparison (wait a few seconds for async refresh to finish)
        time.sleep(20)
        workbook = get_connected_workbook(server, workbook_name)
        server.workbooks.download(workbook.id, filepath=new_path, include_extract=True)
        publish_workbook(server, project_id, new_path, workbook_name_new, creds,
                         as_job=False, skip_connection_check=False)

        print('done!')



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Upload a workbook to Tableau Server.')

    parser.add_argument('--server', type=str, required=True, help='Tableau server to send the requests to, eg "https://10ax.online.tableau.com"')
    parser.add_argument('--site', type=str, required=True, help='Site on the Tableau server, eg "project"')
    parser.add_argument('--project', type=str, required=True, help='Site on the Tableau server, eg "default"')
    parser.add_argument('--workbook', type=str, required=True, help='Workbook file path, eg "./workbooks/workbook1.twbx"')
    # parser.add_argument('--access_token_name', type=str, required=True, help='access token name to use, eg "my-access-token"')
    # parser.add_argument('--access_token_secret', type=str, required=True, help='access token secret, eg "Xbw8qXqHSkiGkVFc9IGHRA==:52Xo6Vo81kurZAXZC3qXtuhhDbXXXilK"')
    parser.add_argument('--username', type=str, required=True, help='username for site')
    parser.add_argument('--password', type=str, required=True, help='password for site')

    args = parser.parse_args()

    main()
