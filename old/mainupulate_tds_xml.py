import glob
import os

from tableau_tools.tableau_documents import TableauFileManager
from tableau_tools.tableau_documents import tableau_connection, \
    tableau_datasource, table_relations, tableau_columns

import tableaudocumentapi as tda


if __name__ == '__main__':
    for workbook_fn in glob.glob('./data/workbooks/*/*.twbx'):
        base_dir = workbook_fn.replace('workbooks', 'datasources_extracted').replace('.twbx', '')

        wb_unofficial = TableauFileManager.open(
            filename=workbook_fn
        )

        wb_official = tda.Workbook(
            filename=workbook_fn
        )

        for i, ds_unofficial in enumerate(wb_unofficial.datasources):
            ds_official = wb_official.datasources[i]
            is_published = 'published' if ds_unofficial.is_published else 'not_published'

            new_dir = os.path.join(base_dir, is_published)
            extracted_ds_fp = os.path.join(new_dir, ds_official.name + '.xml')
            if not os.path.isdir(new_dir):
                os.makedirs(new_dir)

            ds_str = ds_unofficial.get_xml_string()
            with open(extracted_ds_fp, 'w+') as fo:
                fo.write(ds_str.decode('utf-8'))


    tds = TableauFileManager.open(
        filename=extracted_ds_fp
    )

    tds.save_new_file('data/saved_datasource')

    print('done')