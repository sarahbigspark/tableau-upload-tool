import os
import shutil
import zipfile

import tableauserverclient as TSC

from publish_workbook_tsc import ConnectionCredentials
from tableautool.remote.server import Server, publish_datasource
import config as cfg

def create_tdsx_from_dir(path_to_zip, output_file):

    def zipdir(path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(path, '../..')))

    output_fn = os.path.splitext(output_file)[0]
    fn_zip = output_fn + '.zip'
    fn_tdsx = output_fn + '.tdsx'

    zipf = zipfile.ZipFile(fn_zip, 'w', zipfile.ZIP_DEFLATED)
    zipdir(path_to_zip, zipf)
    zipf.close()

    shutil.move(fn_zip, fn_tdsx)
    return fn_tdsx


def main():
    tableau_username = cfg.tableau_username
    tableau_password = cfg.tableau_password
    tableau_site_name = cfg.tableau_site_name
    tableau_server = cfg.tableau_server
    db_username = cfg.db_username
    db_password = cfg.db_password


    workbook_name = 'azuresql_multisource'

    # tableau_auth = TableauAuth(token_name, token_secret, site_id=site_name)
    tableau_auth = TSC.TableauAuth(tableau_username, tableau_password, site_id=tableau_site_name)
    server = Server(tableau_server)
    server.version = '3.8'

    overwrite_true = TSC.Server.PublishMode.Overwrite

    with server.auth.sign_in(tableau_auth):

        data_proj_id = server.get_project_by_name('Data').id

        working_file = 'data/datasources/extract/e10008ec-1c9c-4440-8b68-72289f69d6a4/table1 sarahtableau12345db.tdsx'

        # archive_dir = './data/datasources/extract/manualupdate/table1 sarahtableau12345db_manualupdate'
        archive_dir = './data/datasources/extract/e10008ec-1c9c-4440-8b68-72289f69d6a4/table1 sarahtableau12345db'
        archive_file = './data/datasources/extract/manualupdate/manualupdate2.tdsx'
        archive_file = create_tdsx_from_dir(archive_dir, archive_file)

        connection_credentials = ConnectionCredentials(
            name=db_username,
            password=db_password,
            embed=True,
            oauth=False
        )

        tds_file = 'data/datasources/extract/e10008ec-1c9c-4440-8b68-72289f69d6a4/table1 sarahtableau12345db/table1 (sarahtableau12345db).tds'

        publish_datasource(server, data_proj_id, tds_file, 'manual update',
                           connection_credentials=connection_credentials)