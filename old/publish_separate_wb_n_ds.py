import os
from os.path import join, basename, splitext
from glob import glob
import time
import uuid
import shutil

import tableauserverclient as TSC

import tableautool.remote.server
import utils.tsc_utils as tsu
import config as cfg
from tableautool.local.workbook import LocalWorkbook


def main():
    tb_username = cfg.tableau_username
    tb_password = cfg.tableau_password
    tb_site_name = cfg.tableau_site_name
    tb_server = cfg.tableau_server
    db_username = cfg.db_username
    db_password = cfg.db_password

    source_folder = './test_cases_singledir/multiple-standalonecombo-db-extract-singledb'

    tableau_auth = TSC.TableauAuth(tb_username, tb_password, site_id=tb_site_name)

    server = tableautool.remote.server.Server(tb_server)
    server.version = '3.8'

    with server.auth.sign_in(tableau_auth):
        new_datasources, new_workbooks = do_publish(
            server,
            datasource_files = glob(join(source_folder, '*.tds*')),
            workbook_files = glob(join(source_folder, '*.twb*')),
            ds_project_id = server.get_project_by_name('Data').id,
            wb_project_id = server.get_project_by_name('Reports').id,
            db_credentials = TSC.ConnectionCredentials(name=db_username, password=db_password, embed=True),
            tb_credentials = TSC.ConnectionCredentials(name=tb_username, password=tb_password, embed=True)
        )
        print(f'New workbooks: {new_workbooks} \nNew datasources: {new_datasources}')
        print('done!')

def do_publish(server, datasource_files, workbook_files, ds_project_id, wb_project_id,
               db_credentials:TSC.ConnectionCredentials, tb_credentials:TSC.ConnectionCredentials):

    # publish datasources to destination project
    datasource_ids = publish_datasources(server, ds_project_id, datasource_files, db_credentials)

    # publish workbook to destination project
    workbook_ids = publish_workbooks(server, wb_project_id, ds_project_id, workbook_files, tb_credentials)

    return datasource_ids, workbook_ids

def point_wb_datasources_to_new_project(server, workbook, project_id):
    new_datasources = [server.get_datasource(, None
                       for wb_ds in workbook.datasources]
    workbook.repoint_to_new_published_datasources(new_datasources)
    return workbook

def publish_datasources(server, project_id, datasource_files, ds_credentials):
    datasource_ids = []

    for ds_filepath in datasource_files:
        ds_name = splitext(basename(ds_filepath))[0]
        ds_id = tableautool.remote.server.publish_datasource(server, project_id, ds_filepath, ds_name, ds_credentials, as_job=False)
        datasource_ids.append(ds_id)

    return datasource_ids

def publish_workbooks(server, workbook_project_id, datasource_project_id, workbook_files, tb_credentials):
    workbook_ids = []
    # publish workbook to destination project
    for wb_filepath in workbook_files:
        wb = LocalWorkbook(wb_filepath)
        wb = point_wb_datasources_to_new_project(server, wb, datasource_project_id)

        updated_filepath = str(uuid.uuid1()) + '.twbx'
        wb.save_as(updated_filepath)
        wb_id = tableautool.remote.server.publish_workbook(server, workbook_project_id, updated_filepath, 'manual', tb_credentials)
        os.remove(updated_filepath)
        workbook_ids.append(wb_id)
    return workbook_ids


if __name__=='__main__':
    main()

