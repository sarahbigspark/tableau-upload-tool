import random

text_file = 'data/input_data.csv'

size = 1000
labels = ['red','blue', 'orange']

with open(text_file, 'w+') as fo:
    fo.write('ID,Label\n')
    for i in range(size):
        label = random.choice(labels)
        fo.write(f'{i},{label}\n')

print('done!')