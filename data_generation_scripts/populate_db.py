import random
import os

output_dir = 'data'
text_file = os.path.join(output_dir, 'input_data_large.sql')

if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

tables = ['data', 'table1', 'table2', 'table3', 'table4']

size = 100
labels = ['tralala']

with open(text_file, 'w+') as fo:
    for table in tables:
        fo.write(f"drop table if exists {table} \n")
        fo.write(f"Create table {table} (ID int,Label varchar(20)) \n")
        for i in range(size):
            label = random.choice(labels)
            insert_str = f"insert into {table} values ({i}, '{table}_{label}');\n"
            fo.write(insert_str)

print('done!')