import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(16))
    fullname = Column(String(16))
    nickname = Column(String(16))

    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (
            self.name, self.fullname, self.nickname)


if __name__ == '__main__':

    username = 'admin'
    password = 'WhoCouldHavePredictedThis???'
    aws_endpoint = 'sarah-udemy-mysqlrds1.cjqy7a0fhqil.eu-west-2.rds.amazonaws.com'
    port = '3306'
    dbname = 'sarahdb'
    conn_str = f'mysql+pymysql://{username}:{password}@{aws_endpoint}:{port}/{dbname}'
    engine = create_engine(conn_str)

    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    users = session.query(User).all()

    session.commit()

    print('done!')
