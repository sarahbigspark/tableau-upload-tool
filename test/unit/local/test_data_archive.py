import pytest
import os
from zipfile import ZipFile
from pathlib import Path


from tableautool.local.data_archive import create_data_archive_from_twbx, create_empty_data_archive, \
    unpack_data_from_twbx

PROJ_ROOT_FOLDER = 'tableau-upload-tool'
str_idx = idx = __file__.find(PROJ_ROOT_FOLDER) + len(PROJ_ROOT_FOLDER)
PROJ_ROOT = Path(__file__[:idx])

def test_unpack_data_from_twbx_works_twice_in_same_directory():
    """ test re-runnability: should remove old files"""
    pass

def test_unpack_data_from_twbx(tmpdir):
    workbook_file = PROJ_ROOT / 'test/test_cases_singledir/case3_unpack-datasources_single-db-embedded-extract/before/workbook-single-db-embedded-extract.twbx'

    # do unpack
    unpack_data_from_twbx(workbook_file, tmpdir)

    # list all files for checking
    file_paths = []
    for folder, subs, files in os.walk(tmpdir):
        for filename in files:
            file_paths.append(Path(os.path.abspath(os.path.join(folder, filename))))

    assert file_paths == [tmpdir / 'Data' / 'Extracts' / 'federated_1p5lux301bcaih132bxtp0.hyper']


def test_create_data_archive_from_twbx(tmpdir):
    workbook_file = PROJ_ROOT / 'test/test_cases_singledir/case3_unpack-datasources_single-db-embedded-extract/before/workbook-single-db-embedded-extract.twbx'
    archive_file = tmpdir / 'Data.zip'
    create_data_archive_from_twbx(archive_file, workbook_file)
    zip = ZipFile(archive_file)

    assert zip.namelist() == ['Data/', 'Data/Extracts/', 'Data/Extracts/federated_1p5lux301bcaih132bxtp0.hyper']


def test_create_empty_data_archive(tmpdir):
    archive_file = tmpdir / 'Data.zip'
    create_empty_data_archive(archive_file)

    zip = ZipFile(archive_file)

    assert zip.namelist() == ['Data/', 'Data/Extracts/']
