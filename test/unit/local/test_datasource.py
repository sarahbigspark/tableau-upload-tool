import pytest
import os
import shutil
import zipfile
from zipfile import ZipFile
from pathlib import Path
import xml.etree.ElementTree as ET

from tableautool.local.workbook import LocalWorkbook
from tableautool.local.datasource import DatasourceFactory, EmbeddedDatasource, Datasource, EmbeddedExtractDatasource, PublishedDatasource
from tableautool.local.connection import SqlproxyConnection
from tableautool.utils import unzip

PROJ_ROOT_FOLDER = 'tableau-upload-tool'
str_idx = idx = __file__.find(PROJ_ROOT_FOLDER) + len(PROJ_ROOT_FOLDER)
PROJ_ROOT = Path(__file__[:idx])

class TestDatasourceFactory:

    def test_get_datasource_from_file_tdsx(self):
        tdsx_file = PROJ_ROOT / 'test/unit/test_cases_singledir/case3_unpack-datasources_single-db-embedded-extract/after/table1 (sarahtableau12345db).tdsx'
        ds = DatasourceFactory.get_datasource(tdsx_file)
        assert isinstance(ds, PublishedDatasource)

class TestEmbeddedDatasource:

    def test_datasource_equality(self):

        workbook_file = PROJ_ROOT / 'test/unit/test_cases_singledir/case3_unpack-datasources_single-db-embedded-extract/after/workbook-single-db-published-extract.twbx'

        wb1 = LocalWorkbook(workbook_file)
        ds1 = wb1.datasources[0]

        wb2 = LocalWorkbook(workbook_file)
        ds2 = wb2.datasources[0]

        assert ds1 == ds2

    def test_create_publishable_xml(self, tmpdir):
        workbook_file = PROJ_ROOT / 'test/unit/test_cases_singledir/case3_unpack-datasources_single-db-embedded-extract/before/workbook-single-db-embedded-extract.twbx'
        wb = LocalWorkbook(workbook_file)

        for ds in wb.datasources:
            if isinstance(ds, EmbeddedDatasource):
                orig_items = ds.xml.items()

                publishable_xml = ds._create_publishable_xml()

                expected_publishable_ds_attributes = {
                    'formatted-name' : ds.name,
                    'inline' : 'true',
                    'version' : '18.1',
                    'source-platform' : 'linux',
                    'xml:base' : 'http://localhost:9100',
                    'xmlns:user': 'http://www.tableausoftware.com/xml/user'
                }

                # xml should be ElementTree
                assert type(publishable_xml) == ET.Element
                # check the original embedded datasource did not mutate
                assert ds.xml.items() == orig_items
                # check new values are good
                assert publishable_xml.attrib == expected_publishable_ds_attributes


    def test_save_as_publishable_datasource(self, tmpdir):
        workbook_file = PROJ_ROOT / 'test/unit/test_cases_singledir/case3_unpack-datasources_single-db-embedded-extract/before/workbook-single-db-embedded-extract.twbx'
        wb = LocalWorkbook(workbook_file)

        output_file = tmpdir / 'unpacked_datasource.tdsx'

        for ds in wb.datasources:
            if isinstance(ds, EmbeddedDatasource):
                ds.save_as_publishable_datasource(output_file, workbook_file)
                # check file exists
                assert os.path.exists(output_file)

                # check is archived file
                zipfile.is_zipfile(str(output_file))

                # check contents
                zip = ZipFile(output_file)
                assert zip.namelist() == ['table1 (sarahtableau12345db).tds', 'Data/Extracts/federated_1p5lux301bcaih132bxtp0.hyper']

                # check .tds file exists
                unzip(output_file, tmpdir / 'unpacked_datasource')
                tds_file = tmpdir / 'unpacked_datasource' / 'table1 (sarahtableau12345db).tds'
                assert os.path.exists(tds_file)

                # check .tds file is valid datasource
                xml = ET.parse(str(tds_file)).getroot()
                publishable_ds = DatasourceFactory.get_datasource(xml)
                assert isinstance(publishable_ds, EmbeddedExtractDatasource)


    def test_create_xml_for_repointed_datasource(self, tmpdir, tableau_login_info):
        workbook_file = PROJ_ROOT / 'test/unit/test_cases_singledir/case3_unpack-datasources_single-db-embedded-extract/before/workbook-single-db-embedded-extract.twbx'
        wb = LocalWorkbook(workbook_file)

        for ds in wb.datasources:
            if isinstance(ds, EmbeddedDatasource):
                published_ds_xml = ds.create_xml_for_repointed_datasource(
                    server='10ax.online.tableau.com',
                    site=tableau_login_info['server_name'],
                    content_url=ds.name,
                    datasource_name=ds.caption
                )

                # check new datasource is recognized as published datasource
                published_ds = DatasourceFactory.get_datasource(published_ds_xml)
                assert isinstance(published_ds, PublishedDatasource)

                # check all connections point to sqlproxy connections
                for conn in published_ds.connections:
                    assert isinstance(conn, SqlproxyConnection)

                    expected_connection_attributes = {
                        'channel': 'https',
                        'class': 'sqlproxy',
                        'dbname': ds.name,
                        'directory': 'dataserver',
                        'port': '443',
                        'saved-credentials-viewerid': '79910',
                        'server': '10ax.online.tableau.com',
                        'server-ds-friendly-name': ds.caption
                    }
                    assert conn.xml.attrib == expected_connection_attributes

                # check extract removed
                extracts = published_ds.xml.findall('extract')
                assert extracts == []

                # check references to extracts removed from object relations
                for rel in published_ds.xml.findall('.//relation'):
                    for k in rel.attrib:
                        assert 'Extract' not in rel.attrib[k]



@pytest.fixture(scope='module')
def tableau_login_info():
    return {
        'username' : 'sarah.scott@bigspark.dev',
        'password' : '1A8VNmFJRAKe!',
        'site_name' : 'sarahtableaudev2dev840068',
        'server_url' : 'https://10ax.online.tableau.com',
        'server_name' : '10ax.online.tableau.com'
    }