import pytest
import os
from zipfile import ZipFile
from pathlib import Path

from tableautool.local.workbook import LocalWorkbook
from tableautool.local.datasource import DatasourceFactory, EmbeddedDatasource
from tableautool.local.data_archive import create_data_archive_from_twbx, create_empty_data_archive, unpack_data_from_twbx

PROJ_ROOT_FOLDER = 'tableau-upload-tool'
str_idx = idx = __file__.find(PROJ_ROOT_FOLDER) + len(PROJ_ROOT_FOLDER)
PROJ_ROOT = Path(__file__[:idx])


