import pytest
from copy import deepcopy
from xml.etree import ElementTree as ET

from tableautool.utils import XMLComparison

class TestXMLComparison:
    def test_single_depth(self):
        xml1 = ET.Element('tag1')
        xml1.set('field1', 'lala')
        xml1.set('field2', 'lala')

        xml2 = ET.Element('tag1')
        xml2.set('field1', 'lolo')
        xml2.set('field2', 'lolo')

        result = XMLComparison(xml1, xml2)

        assert result.mismatch_tag == []
        assert result.mismatch_text == []
        assert result.mismatch_tail == []
        assert result.mismatch_length == []
        assert len(result.mismatch_attrib) == 1
        assert list(result.mismatch_attrib[0].keys()) == ['xml1.tag1', 'xml2.tag1']

    def test_depth2(self):
        xml1 = ET.Element('tag1')
        xml1.set('field1', 'lala')
        xml1.set('field2', 'lala')
        xml1_1 = ET.Element('subtag1')
        xml1.append(xml1_1)

        xml2 = ET.Element('tag1')
        xml2.set('field1', 'lala')
        xml2.set('field2', 'lala')

        result = XMLComparison(xml1, xml2)

        assert result.mismatch_tag == []
        assert result.mismatch_text == []
        assert result.mismatch_tail == []
        assert result.mismatch_attrib == []
        assert len(result.mismatch_length) == 1
        assert list(result.mismatch_length[0].keys()) == ['xml1.tag1', 'xml2.tag1']


    def test_depth2_ordering(self):
        xml1 = ET.Element('tag1')
        xml1.set('field1', 'lala')
        xml1.set('field2', 'lala')
        xml1_1 = ET.Element('subtag1')
        xml1_2 = ET.Element('subtag2')
        xml1.insert(1, xml1_1)
        xml1.insert(2, xml1_2)

        xml2 = ET.Element('tag1')
        xml2.set('field1', 'lala')
        xml2.set('field2', 'lala')
        xml2_1 = ET.Element('subtag1')
        xml2_2 = ET.Element('subtag2')
        xml2.insert(2, xml2_1)
        xml2.insert(1, xml2_2)

        result = XMLComparison(xml1, xml2)

        assert result.mismatch_tag == []
        assert result.mismatch_text == []
        assert result.mismatch_tail == []
        assert result.mismatch_attrib == []
        assert result.mismatch_length == []
        assert result.mismatch_attrib == []

    def test_depth3_child_order(self):

        xml1 = ET.Element('tag1')
        xml1.set('field1', 'lala')
        xml1.set('field2', 'lala')
        xml1_1 = ET.Element('subtag1')
        xml1_2 = ET.Element('subtag2')
        xml1_1_1 = ET.Element('subsubtag1')
        xml1_1_2 = ET.Element('subsubtag2')
        xml1_1_2.set('field3', 'some_value')
        xml1_1.insert(1, xml1_1_1)
        xml1_1.insert(2, xml1_1_2)
        xml1.insert(1, xml1_1)
        xml1.insert(2, xml1_2)

        xml2 = deepcopy(xml1)
        subtag = xml2.find('subtag1')
        tag_to_move = subtag.find('subsubtag1')
        subtag.remove(tag_to_move)
        subtag.append(tag_to_move)

        result = XMLComparison(xml1, xml2)

        assert result.objects_match

    def test_depth3_attribute_order(self):

        xml1 = ET.Element('tag1')
        xml1.set('field1', 'lala')
        xml1.set('field2', 'lolo')
        xml1.set('field3', 'lele')

        xml2 = ET.Element('tag1')
        xml2.set('field3', 'lele')
        xml2.set('field2', 'lolo')
        xml2.set('field1', 'lala')

        result = XMLComparison(xml1, xml2)

        assert result.objects_match



    def test_depth3_value(self):
        xml1 = ET.Element('tag1')
        xml1.set('field1', 'lala')
        xml1.set('field2', 'lala')
        xml1_1 = ET.Element('subtag1')
        xml1_2 = ET.Element('subtag2')
        xml1_1_1 = ET.Element('subsubtag1')
        xml1_1_2 = ET.Element('subsubtag2')
        xml1_1_2.set('field3', 'some_value')
        xml1_1.insert(1, xml1_1_1)
        xml1_1.insert(2, xml1_1_2)
        xml1.insert(1, xml1_1)
        xml1.insert(2, xml1_2)

        xml2 = ET.Element('tag1')
        xml2.set('field1', 'lala')
        xml2.set('field2', 'lala')
        xml2_1 = ET.Element('subtag1')
        xml2_2 = ET.Element('subtag2')
        xml2_1_1 = ET.Element('subsubtag1')
        xml2_1_2 = ET.Element('subsubtag2')
        xml2_1_2.set('field3', 'some_other_value')
        xml2_1.insert(1, xml2_1_1)
        xml2_1.insert(2, xml2_1_2)
        xml2.insert(1, xml2_1)
        xml2.insert(2, xml2_2)

        result = XMLComparison(xml1, xml2)

        assert result.mismatch_tag == []
        assert result.mismatch_text == []
        assert result.mismatch_tail == []
        assert len(result.mismatch_attrib) == 1
        expected_mismatch_tags = ['xml1.tag1.subtag1.subsubtag2', 'xml2.tag1.subtag1.subsubtag2']
        assert list(result.mismatch_attrib[0].keys()) == expected_mismatch_tags
        assert result.mismatch_length == []
