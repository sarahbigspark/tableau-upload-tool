import os
from glob import glob
import shutil
import zipfile
from zipfile import ZipFile
from pathlib import Path

import pytest
import tableauserverclient as TSC

from tableautool.local.workbook import LocalWorkbook
from tableautool.local.datasource import EmbeddedDatasource, DatasourceFactory, PublishedDatasource
from tableautool.remote.server import Server
import tableautool.utils

TEST_PROJ = 'Test'
TEST_DATA_DIR = Path('test_cases')
TEST_TEMPLATE_FOLDERS = os.listdir(TEST_DATA_DIR)

workbook_template_folders = [x for x in TEST_TEMPLATE_FOLDERS if ('workbook' in x) or ('combo' in x)]
embedded_testcases = [x for x in TEST_TEMPLATE_FOLDERS if 'embedded' in x]
embedded_database_testcases = [x for x in embedded_testcases if 'db' in x]
embedded_extract_testcases = [x for x in embedded_testcases if 'extract' in x]
embedded_live_testcases = [x for x in embedded_testcases if 'live' in x]



class TestWorkbookUnpack:

    @pytest.mark.parametrize('test_case', embedded_testcases)
    def test_workbook_unpack(self, tmpdir, test_case):
        """
        Test goes through all workbook (.twbx) files in 'before' folder, unpacks the
        datasources to separate files, and compares the generated files to the expected
        results stored in the 'after' folder.
        Test checks for equality of xml and for file names in .tdsx archives.
        :param tmpdir:
        :param test_case:
        :return:
        """
        workbook_files = glob(str(TEST_DATA_DIR / test_case / 'before/*.twbx'))

        ds_files_expected = glob(str(TEST_DATA_DIR / test_case / 'after/*.tdsx'))
        ds_files_actual = []

        for workbook_file in workbook_files:
            wb = LocalWorkbook(workbook_file)
            for ds in wb.datasources:
                if isinstance(ds, EmbeddedDatasource):
                    ds_file = str(tmpdir / ds.caption + '.tdsx')
                    ds.save_as_publishable_datasource(ds_file, workbook_file)
                    ds_files_actual.append(ds_file)

        for ds_file_expected in ds_files_expected:
            ds_file_actual = str(tmpdir / os.path.basename(ds_file_expected))
            assert os.path.isfile(ds_file_actual)

            ds_expected = DatasourceFactory.get_datasource(ds_file_expected)
            ds_actual = DatasourceFactory.get_datasource(ds_file_actual)

            assert ds_actual.data_files == ds_expected.data_files
            assert ds_actual == ds_expected



    @pytest.mark.parametrize('test_folder', embedded_testcases)
    def test_workbook_unpacked_datasources_publish(self, server, tmpdir, test_folder, datasource_connection_credentials):
        workbook_files = glob(str(TEST_DATA_DIR / test_folder / 'before/*.twbx'))

        for workbook_file in workbook_files:
            wb = LocalWorkbook(workbook_file)
            for ds in wb.datasources:
                if isinstance(ds, EmbeddedDatasource):
                    ds_file = tmpdir / ds.caption + '.tdsx'
                    ds.save_as_publishable_datasource(ds_file, workbook_file)

                    ds_id = tableautool.remote.server.publish_datasource(
                        server=server,
                        project_id=server.get_project_by_name(name=TEST_PROJ).id,
                        file_path=ds_file,
                        new_datasource_name=ds.caption,
                        connection_credentials=datasource_connection_credentials[ds.connection_key]
                    )

                    server_ds = server.get_datasource(id=ds_id)
                    server.datasources.delete(server_ds.id)



    @pytest.mark.parametrize('test_folder', embedded_extract_testcases)
    def test_workbook_unpacked_datasources_refresh(self, server, tmpdir, test_folder, datasource_connection_credentials):
        workbook_files = glob(str(TEST_DATA_DIR / test_folder / 'before/*.twbx'))

        datasource_ids = []

        for workbook_file in workbook_files:
            wb = LocalWorkbook(workbook_file)
            for ds in wb.datasources:
                if isinstance(ds, EmbeddedDatasource):
                    ds_file = tmpdir / ds.caption + '.tdsx'
                    ds.save_as_publishable_datasource(ds_file, workbook_file)

                    ds_id = tableautool.remote.server.publish_datasource(
                        server=server,
                        project_id=server.get_project_by_name(name=TEST_PROJ).id,
                        file_path=ds_file,
                        new_datasource_name=ds.caption,
                        connection_credentials=datasource_connection_credentials[ds.connection_key]
                    )
                    datasource_ids.append(ds_id)

                    ds_before_refresh = server.get_datasource(id=ds_id)
                    ds_before_refresh.refresh()
                    ds_after_refresh = server.get_datasource(id=ds_id)

                    assert ds_before_refresh.updated_at < ds_after_refresh.updated_at

                    server_ds = server.get_datasource(id=ds_id)
                    server.datasources.delete(server_ds.id)


    @pytest.mark.parametrize('test_folder', embedded_testcases)
    def test_workbook_unpacked_datasources_refresh(self, server, tmpdir, test_folder, datasource_connection_credentials, tableau_login_info, tableau_connection_credentials):
        workbook_files = glob(str(TEST_DATA_DIR / test_folder / 'before/*.twbx'))

        datasource_ids = []

        for workbook_file in workbook_files:
            wb = LocalWorkbook(workbook_file)
            for i, ds in enumerate(wb.datasources):
                if isinstance(ds, EmbeddedDatasource):
                    ds_file = tmpdir / ds.caption + '.tdsx'
                    ds.save_as_publishable_datasource(ds_file, workbook_file)

                    ds_id = tableautool.remote.server.publish_datasource(
                        server=server,
                        project_id=server.get_project_by_name(name=TEST_PROJ).id,
                        file_path=ds_file,
                        new_datasource_name=ds.caption,
                        connection_credentials=datasource_connection_credentials[ds.connection_key]
                    )
                    datasource_ids.append(ds_id)
                    server_ds = server.get_datasource(id=ds_id)

                    repointed_xml = ds.create_xml_for_repointed_datasource(
                        server=tableau_login_info['server_name'],
                        site=tableau_login_info['site_name'],
                        content_url=server_ds.content_url,
                        datasource_name=server_ds.name
                    )
                    repointed_ds = DatasourceFactory.get_datasource(repointed_xml)
                    wb.replace_datasource(i, repointed_ds)
                    wb.datasources[i] = repointed_ds

            assert all([isinstance(ds, PublishedDatasource) for ds in wb.datasources])

            updated_wb_file = tmpdir / 'repointed_workbook.twbx'
            wb.save_as(updated_wb_file)

            wb_id = tableautool.remote.server.publish_workbook(
                server=server,
                project_id=server.get_project_by_name(name=TEST_PROJ).id,
                file_path=updated_wb_file,
                new_workbook_name='repointed_workbook',
                connection_credentials=tableau_connection_credentials
            )

            server.workbooks.delete(wb_id)

        for ds_id in datasource_ids:
            server.datasources.delete(ds_id)





@pytest.fixture
def tableau_login_info():
    return {
        'username' : 'sarah.scott@bigspark.dev',
        'password' : '1A8VNmFJRAKe!',
        'site_name' : 'sarahtableaudev2dev840068',
        'server_url' : 'https://10ax.online.tableau.com',
        'server_name' : '10ax.online.tableau.com'
    }

@pytest.fixture
def tableau_connection_credentials(tableau_login_info):
    return TSC.ConnectionCredentials(
        name=tableau_login_info['username'],
        password=tableau_login_info['password'],
        embed=True
    )

@pytest.fixture
def azuredb_login_info():
    return {
        'server': 'sarahtableau12345.database.windows.net',
        'database' : 'sarahtableau12345db',
        'username' : 'sqladmin',
        'password' : 'WhoCouldHavePredictedThis???'
    }

@pytest.fixture
def azuredb_connection(azuredb_login_info):
    return TSC.ConnectionCredentials(
        name=azuredb_login_info['username'],
        password=azuredb_login_info['password'],
        embed=True
    )

@pytest.fixture
def awsdb_login_info():
    return {
        'server': 'sarah-tableau-mysql-db.cjqy7a0fhqil.eu-west-2.rds.amazonaws.com',
        'database' : 'awstableaudb',
        'username' : 'admin',
        'password' : 'H3yD4y!!'
    }

@pytest.fixture
def awsdb_connection(awsdb_login_info):
    return TSC.ConnectionCredentials(
        name=awsdb_login_info['username'],
        password=awsdb_login_info['password'],
        embed=True
    )

@pytest.fixture
def datasource_connection_credentials(azuredb_connection, awsdb_connection):
    return {
        'azure_sqldb:sarahtableau12345db' : azuredb_connection,
        'mysql:awstableaudb' : azuredb_connection
    }



@pytest.fixture(autouse=True)
def server(tableau_login_info):
    tableau_auth = TSC.TableauAuth(
        tableau_login_info['username'],
        tableau_login_info['password'],
        tableau_login_info['site_name']
    )

    tb_server = tableautool.remote.server.Server(tableau_login_info['server_url'])
    tb_server.version = '3.8'

    with tb_server.auth.sign_in(tableau_auth):
        yield tb_server
