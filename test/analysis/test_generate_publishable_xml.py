import pytest
import os
from pathlib import Path

import tableauserverclient as TSC

import tableautool
from tableautool.local.workbook import LocalWorkbook
from tableautool.local.datasource import DatasourceFactory, EmbeddedDatasource

TEST_PROJ = 'Test'

PROJ_ROOT_FOLDER = 'tableau-upload-tool'
str_idx = idx = __file__.find(PROJ_ROOT_FOLDER) + len(PROJ_ROOT_FOLDER)
PROJ_ROOT = Path(__file__[:idx])


class TestGeneratePublishableXML:

    def test_create_publishable_xml(self, server, datasource_connection_credentials, tableau_login_info):
        workbook_file = PROJ_ROOT / 'test/analysis/test_cases_analysis/case3_unpack-datasources_single-db-embedded-extract/before/workbook-single-db-embedded-extract.twbx'
        wb = LocalWorkbook(workbook_file)

        generated_ds_tds = PROJ_ROOT / 'test/analysis/test_cases_analysis/case3_unpack-datasources_single-db-embedded-extract/generated/generated.tds'
        generated_ds_tdsx = PROJ_ROOT / 'test/analysis/test_cases_analysis/case3_unpack-datasources_single-db-embedded-extract/generated/generated.tdsx'

        for ds in wb.datasources:
            if isinstance(ds, EmbeddedDatasource):
                publishable_xml = ds._create_publishable_xml()
                publishable_ds = DatasourceFactory.get_datasource(publishable_xml)

                publishable_ds.save_as(generated_ds_tdsx)

                published_datasource_id = tableautool.remote.server.publish_datasource(
                    server=server,
                    project_id=server.get_project_by_name(name=TEST_PROJ).id,
                    file_path=generated_ds_tdsx,
                    new_datasource_name='extracted',
                    connection_credentials=datasource_connection_credentials,
                    as_job=False
                )

                server_ds = server.get_datasource(id=published_datasource_id)
                server_ds.refresh()



@pytest.fixture
def csv_before(tmp_path):
    d = tmp_path / "sub"
    if not os.path.isdir(d):
        d.mkdir()

    data = "ID,Count\nBeforeUpdate,1\n"
    csv = d / "data_before.csv"
    csv.write_text(data)
    yield csv

@pytest.fixture
def csv_after(tmp_path):
    d = tmp_path / "sub"
    if not os.path.isdir(d):
        d.mkdir()

    data = "ID,Count\nAfterUpdate,1\n"
    csv = d / "data_after.csv"
    csv.write_text(data)
    yield csv



@pytest.fixture
def tableau_login_info():
    return {
        'username' : 'sarah.scott@bigspark.dev',
        'password' : '1A8VNmFJRAKe!',
        'site_name' : 'sarahtableaudev2dev840068',
        'server_url' : 'https://10ax.online.tableau.com'
    }

@pytest.fixture
def tableau_connection_credentials(tableau_login_info):
    return TSC.ConnectionCredentials(
        name=tableau_login_info['username'],
        password=tableau_login_info['password'],
        embed=True
    )

@pytest.fixture
def database_login_info():
    return {
        'server': 'sarahtableau12345.database.windows.net',
        'database' : 'sarahtableau12345db',
        'username' : 'sqladmin',
        'password' : 'WhoCouldHavePredictedThis???'
    }

@pytest.fixture
def database_connection_credentials(database_login_info):
    return TSC.ConnectionCredentials(
        name=database_login_info['username'],
        password=database_login_info['password'],
        embed=True
    )

@pytest.fixture(autouse=True)
def server(tableau_login_info):
    tableau_auth = TSC.TableauAuth(
        tableau_login_info['username'],
        tableau_login_info['password'],
        tableau_login_info['site_name']
    )

    tb_server = tableautool.remote.server.Server(tableau_login_info['server_url'])
    tb_server.version = '3.8'

    with tb_server.auth.sign_in(tableau_auth):
        yield tb_server
