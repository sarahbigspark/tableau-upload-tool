import pytest
import os
from pathlib import Path
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import Sequence
from sqlalchemy import create_engine, select, MetaData, Table, and_
import tableauserverclient as TSC

import tableautool
from tableautool.local.workbook import LocalWorkbook
from tableautool.local.datasource import EmbeddedDatasource, DatasourceFactory, PublishedDatasource
from tableautool.remote.server import Server
import tableautool.utils

TEST_PROJ = 'Test'
TEST_DATA_DIR = Path('test_cases')
TEST_TEMPLATE_FOLDERS = os.listdir(TEST_DATA_DIR)

class TestEndToEnd:
    def test_end_to_end(self, server, db_inserts_generator, sqlalchemy_engine, datasource_connection_credentials):

        workbook_file = 'test_cases/case1_workbook-db-embedded-extract-aws-users/workbook-awsdb-users-table.twbx'
        wb = LocalWorkbook(workbook_file)

        wb_id = tableautool.remote.server.publish_workbook(
            server=server,
            project_id=server.get_project_by_name(name=TEST_PROJ).id,
            file_path=workbook_file,
            new_workbook_name='workbook',
            connection_credentials=datasource_connection_credentials[wb.datasources[0].connection_key]
        )

        wb_server = server.get_workbook(id=wb_id)
        server.workbooks.populate_views(wb_server)
        view = wb_server.views[0]
        server.views.populate_csv(view)
        print(''.join([str(x) for x in view.csv]))

        for db_insert in db_inserts_generator:
            new_user = db_insert
            wb_server.refresh()
            wb_server = server.get_workbook(name='workbook', project_id=server.get_project_by_name(name=TEST_PROJ).id)
            server.workbooks.populate_views(wb_server)
            view_id = wb_server.views[0].id
            view = server.views.get_by_id(view_id)
            server.views.populate_csv(view)
            print(''.join([str(x) for x in view.csv]))

        stmt = select([users_table.columns.firstname, users_table.columns.surname])

        connection = sqlalchemy_engine.connect()
        results = connection.execute(stmt).fetchall()
        user1 = db_inserts_generator.__next__()




        return

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column('user_id', Integer, primary_key=True)
    firstname = Column(String(16))
    surname = Column(String(16))
    email = Column(String(16))

    def __repr__(self):
        return "<User(firstname='%s', surname='%s', email='%s')>" % (
            self.firstname, self.surname, self.email)

@pytest.fixture
def azuredb_login_info():
    return {
        'server': 'sarahtableau12345.database.windows.net',
        'database' : 'sarahtableau12345db',
        'username' : 'sqladmin',
        'password' : 'WhoCouldHavePredictedThis???'
    }

@pytest.fixture
def azuredb_connection(azuredb_login_info):
    return TSC.ConnectionCredentials(
        name=azuredb_login_info['username'],
        password=azuredb_login_info['password'],
        embed=True
    )

@pytest.fixture
def awsdb_login_info():
    return {
        'server': 'sarah-tableau-mysql-db.cjqy7a0fhqil.eu-west-2.rds.amazonaws.com',
        'database' : 'awstableaudb',
        'username' : 'admin',
        'password' : 'H3yD4y!!',
        'port' : '3306'
    }

@pytest.fixture
def awsdb_connection(awsdb_login_info):
    return TSC.ConnectionCredentials(
        name=awsdb_login_info['username'],
        password=awsdb_login_info['password'],
        embed=True
    )

@pytest.fixture
def datasource_connection_credentials(azuredb_connection, awsdb_connection):
    return {
        'azure_sqldb:sarahtableau12345db' : azuredb_connection,
        'mysql:awstableaudb' : awsdb_connection
    }

@pytest.fixture
def sqlalchemy_engine(awsdb_login_info):
    username = awsdb_login_info['username']
    password = awsdb_login_info['password']
    server_name = awsdb_login_info['server']
    port = awsdb_login_info['port']
    dbname = awsdb_login_info['database']
    conn_str = f'mysql+pymysql://{username}:{password}@{server_name}:{port}/{dbname}'
    return create_engine(conn_str)

@pytest.fixture
def db_inserts_generator(sqlalchemy_engine):
    engine = sqlalchemy_engine

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    with Session() as session:
        # session = Session()

        user1 = User(
            firstname='John',
            surname='Doe',
            email='john@doe.es')

        user2 = User(
            firstname='Jack',
            surname='Doe',
            email='jack@doe.es')

        user3 = User(
            firstname='Jane',
            surname='Doe',
            email='jane@doe.es')

    def do_insert():
        for user in [user1, user2, user3]:
            with Session() as session:
                session.add(user)
                session.commit()

            yield user

    yield do_insert()

@pytest.fixture
def users_table():
    metadata = MetaData(bind=None)
    users_table = Table(
        'users',
        metadata,
        autoload=True,
        autoload_with=sqlalchemy_engine
    )
    return users_table

@pytest.fixture
def awsdb_update2():
    pass

@pytest.fixture
def aws_update3():
    pass









@pytest.fixture(autouse=True)
def server(tableau_login_info):
    tableau_auth = TSC.TableauAuth(
        tableau_login_info['username'],
        tableau_login_info['password'],
        tableau_login_info['site_name']
    )

    tb_server = tableautool.remote.server.Server(tableau_login_info['server_url'])
    tb_server.version = '3.8'

    with tb_server.auth.sign_in(tableau_auth):
        yield tb_server

@pytest.fixture
def tableau_login_info():
    return {
        'username' : 'sarah.scott@bigspark.dev',
        'password' : '1A8VNmFJRAKe!',
        'site_name' : 'sarahtableaudev2dev840068',
        'server_url' : 'https://10ax.online.tableau.com',
        'server_name' : '10ax.online.tableau.com'
    }

@pytest.fixture
def tableau_connection_credentials(tableau_login_info):
    return TSC.ConnectionCredentials(
        name=tableau_login_info['username'],
        password=tableau_login_info['password'],
        embed=True
    )